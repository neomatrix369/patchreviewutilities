package org.ljc.adoptojdk;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants.*;

public final class DirectoryFileUtility {
	
	private DirectoryFileUtility() {
	}

	public static String dropTrailingSeparator(String pathWithFilename, String directorySeparator) {
		if (hasTrailingSeparator(pathWithFilename, directorySeparator)) {
			return pathWithFilename.substring(0, pathWithFilename.lastIndexOf(directorySeparator));
		} else {
			return pathWithFilename;
		}
	}

	public static boolean hasTrailingSeparator(String inputPath, String separator) {
		return inputPath.endsWith(separator);
	}
	
	public static boolean filenameHasBeenRemoved(String pathWithoutFilename, String pathString) {
		return !pathWithoutFilename.equals(pathString);
	}

	public static boolean isInvalidPath(String inPathString) {
		for (char eachChar : ILLEGAL_CHARACTERS) {
			if (inPathString.contains(String.valueOf(eachChar))) {
				return true;
			}
		}
		return false;
	}

	public static String removeFilenameFromPath(String pathWithFilename, String directorySeparator) {
		
		String extractedFilename = extractFilename(pathWithFilename);
		
		if (extractedFilename.isEmpty()) {
			return pathWithFilename;
		} else {		
			int startPos = 0;
			int endPos = pathWithFilename.length() - extractedFilename.length();
			return pathWithFilename.substring(startPos, endPos);
		}
	}

	public static String extractFilename(String inPathWithFilename) {
		File file = new File(inPathWithFilename);
		return file.getName();
	}

	public static List<String> sortGiven(List<String> unsortedItems,
			AbstractSortOrder sortOrder) {
		List<String> workingList = new ArrayList<String>(unsortedItems);
		Collections.sort(workingList, sortOrder);
		return workingList;
	}

	public static AbstractSortOrder inDescendingOrderByLength() {
		return new DescendingOrderByTokenLength();
	}
	
	public static AbstractSortOrder inAscendingOrderByName() {
		return new AscendingOrderByTokenName();
	}
	
	public static List<String> removeDuplicateItems(List<String> duplicateItems) {
		List<String> localDuplicateItems = new ArrayList<String>();
		Set<String> workingHashSet = new HashSet<String>();
		workingHashSet.addAll(duplicateItems);
		localDuplicateItems.addAll(workingHashSet);
		return localDuplicateItems;
	}
}

abstract class AbstractSortOrder implements Comparator<String> {}

class DescendingOrderByTokenLength extends AbstractSortOrder {
	@Override
	public int compare(String firstStringToken, String secondStringToken) {
		if (firstStringToken.length() < secondStringToken.length()) {
			return 1;
		} else if(firstStringToken.length() > secondStringToken.length()) {
			return -1;
		} else {
			return 0;
		}
	}
}

class AscendingOrderByTokenName extends AbstractSortOrder {
	@Override
	public int compare(String firstToken, String secondToken) {
		return firstToken.compareTo(secondToken);
	}
}