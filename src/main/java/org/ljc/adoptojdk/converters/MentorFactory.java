package org.ljc.adoptojdk.converters;

import models.*;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.ljc.adoptojdk.converters.CommonFunctions.COMMA_SEPARATOR;
import static org.ljc.adoptojdk.converters.CommonFunctions.NEW_LINE_CHAR;

public final class MentorFactory {
	private MentorFactory() {
	}

	private static final int MENTOR_FIELD_MENTOR_TYPE = 4;
	private static final int MENTOR_FIELD_EMAIL = 3;

	private static final int MENTOR_FIELD_NAME = 2;

	private static final int MENTOR_INTEREST_FIELD_PROJECT = 1;
	private static final int MENTOR_INTEREST_FIELD_PATH = 0;

    private static final int MAX_FIELDS = 5;

    private static final String ERROR_MESSAGE = "Error due to: %s%n";

	public static Mentors<Mentor> addMentorOnlyIfUnique(Mentors mentorsList,
			Mentor newMentor) {
        Mentors<Mentor> existingMentors = mentorsList;
        if (newMentor == null) {
            return existingMentors;
        }

        String email = newMentor.email;
		boolean addNewMentor = true;

        int lastId = existingMentors.size();

		for (Mentor eachExistingMentor : existingMentors) {
            if (eachExistingMentor.email.trim().isEmpty()) {
                addNewMentor = false;
            }  else if (eachExistingMentor.email.equalsIgnoreCase(email)) {
				addNewMentor = false;

				List<Interest> newInterests = addInterestOnlyIfUnique(
						eachExistingMentor, newMentor.interests);

				if (newInterests.size() > 0) {
					eachExistingMentor.interests.addAll(newInterests);

					int index = existingMentors.indexOf(eachExistingMentor);
					existingMentors.set(index, eachExistingMentor);
				}
			}
		}

		if (addNewMentor) {
            newMentor.id = lastId + 1;
			existingMentors.add(newMentor);
		}

		return existingMentors;
	}

	private static List<Interest> addInterestOnlyIfUnique(
			Mentor existingMentor, List<Interest> newMentorInterests) {
		List<Interest> newInterests = new ArrayList<Interest>();

        int lastId = 0;
        if (existingMentor.interests != null) {
            lastId = existingMentor.interests.size();
        }

		for (Interest eachMentorInterest : newMentorInterests) {
			if (!eachMentorInterest.path.trim().isEmpty()) {
                boolean doesNotExist = ! interestExistsInTheList(eachMentorInterest.path, existingMentor.interests);
                if (doesNotExist) {
                    lastId++;
                    eachMentorInterest.id = lastId;
				    newInterests.add(eachMentorInterest);
			    }
            }
		}

		return newInterests;
	}

	private static boolean interestExistsInTheList(String path,
			List<Interest> interests) {
		for (Interest eachInterest : interests) {
			if (eachInterest.path.equalsIgnoreCase(path)) {
				return true;
			}
		}
		return false;
	}

	public static Mentor createMentorClassesUsingCSVAsListOfStringsArrays(
            List<String[]> csvLines, int targetIndex, int id) {
		Interests interests = new Interests();

        if (targetIndex >= csvLines.size()) {
            return null;
        }

		String[] internalCSVLine = csvLines.get(targetIndex);
		if (internalCSVLine == null) {
			return null;
		}

		Mentor mentor = null;
		if (internalCSVLine.length == MAX_FIELDS) {
			mentor = new Mentor();
			Interest interest = new Interest();

            interest.id = 1;
			interest.path = internalCSVLine[MENTOR_INTEREST_FIELD_PATH].trim();
			interest.project = internalCSVLine[MENTOR_INTEREST_FIELD_PROJECT];

            if (!interest.path.isEmpty()) {
			    interests.add(interest);
            }

            mentor.id = id;
			mentor.name = stripOffDelimeters(internalCSVLine[MENTOR_FIELD_NAME]).trim();
			mentor.email = stripOffDelimeters(internalCSVLine[MENTOR_FIELD_EMAIL]).trim();

            if (mentor.name.isEmpty() || mentor.email.isEmpty()) {
                return null;
            }

			String mentorTypeString = internalCSVLine[MENTOR_FIELD_MENTOR_TYPE];
			mentorTypeString = mentorTypeString.toUpperCase();
	
			mentor.mentorType = MentorType.fromString(mentorTypeString);
			mentor.interests = interests;
		}

		return mentor;
	}

	public static Mentor createMentorClassesUsingCSV(String csvFieldValues,
			int index) {
		String[] csvLinesAsArray = csvFieldValues.split(NEW_LINE_CHAR);
		List<String[]> csvLineStringList = new ArrayList<String[]>();
		for (String eachCSVLine : csvLinesAsArray) {
			csvLineStringList.add(eachCSVLine.split(COMMA_SEPARATOR));
		}

		return createMentorClassesUsingCSVAsListOfStringsArrays(
				csvLineStringList, index, 1);
	}

	private static String stripOffDelimeters(String text) {
		String localText = text.replaceAll("'", "");
		localText = text.replaceAll("\"", "");
		return localText;
	}

	private static String extractFieldValue(String eachCSVLine,
			String separator, int fieldIndex) {
		return eachCSVLine.split(String.valueOf(separator))[fieldIndex];
	}

    public static void writeBackOutputToFile(String actualText,
                                            String filenameWithLocation) {
        PrintWriter out = null;
        try {
            try {
                out = new PrintWriter(filenameWithLocation);
                out.println(actualText);
            } catch (FileNotFoundException ex) {
                System.out.format(ERROR_MESSAGE, ex);
            }
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
