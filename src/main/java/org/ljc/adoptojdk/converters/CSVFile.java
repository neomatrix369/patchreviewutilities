package org.ljc.adoptojdk.converters;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

import static org.ljc.adoptojdk.converters.CommonFunctions.*;

public final class CSVFile {
	private String filename;
	private String separator;

	public CSVFile(String filename, String separator) {
		this.filename = filename;
		this.separator = separator;
	}

	public String[] readFirstLineOfCSV() throws IOException {
		String[] firstCSVLineReadFromFile = new String[0];
		List<String[]> listOfCSVEntries = readCSVfile();
		if (listOfCSVEntries.size() > 0) {
			firstCSVLineReadFromFile = listOfCSVEntries.get(0);
		}
		return firstCSVLineReadFromFile;
	}

	public List<String[]> readCSVfile() throws IOException {
		CSVReader csvReader = new CSVReader(new FileReader(filename), separator.charAt(0));
		List<String[]> listOfCSVEntries = new ArrayList<String[]>();
		try {
			listOfCSVEntries = csvReader.readAll();
		} finally {
			csvReader.close();
		}
		return listOfCSVEntries;
	}

	public String createCSVLinesFromListOfArrays(
			List<String[]> listOfStringArrays, String separator) {
		int startIndex = 0, endIndex = listOfStringArrays.size();
		return createCSVLinesFromListOfArrays(listOfStringArrays, separator,
				startIndex, endIndex);
	}

	public String createCSVLinesFromListOfArrays(
			List<String[]> listOfStringArrays, String separator, int startIndex,
			int endIndex) {
		String result = "";
		int indexCtr = 0;
		for (String[] eachArray : listOfStringArrays) {
			if ((indexCtr >= startIndex) && (indexCtr <= endIndex)) {
				result = result + createCSVLineFromArray(eachArray, separator)
						+ NEW_LINE_CHAR;
			}

			indexCtr++;
		}
		result = removeTrailingChar(result, NEW_LINE_CHAR);
		return result;
	}

	private String removeTrailingChar(String inputString, String separator) {
		String result = inputString;
		if ((result != null) && (!result.isEmpty())) {
			String lastChar = result.substring(result.length() - 1, result.length());
			if (lastChar.equalsIgnoreCase(separator)) {
				result = result.substring(0, result.length() - 1);
			}
		}
		return result;
	}
	
	public String createCSVLineFromArray(String[] arrayOfElements,
			String separator) {
		String result = "";
		for (String eachElement : arrayOfElements) {
			if (hasMoreThanOneWord(eachElement)) {
				result = result + ESCAPED_SINGLE_QUOTE + eachElement + ESCAPED_SINGLE_QUOTE + separator;
			} else {
				result = result + eachElement + separator;
			}
		}

		if (result.length() > 0) {
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}	
}
