package org.ljc.adoptojdk.converters;

import com.esotericsoftware.yamlbeans.YamlWriter;
import models.Mentor;
import models.Mentors;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static org.ljc.adoptojdk.converters.CommonFunctions.COMMA_SEPARATOR;
import static org.ljc.adoptojdk.converters.CommonFunctions.readFromFile;
import static org.ljc.adoptojdk.converters.MentorFactory.addMentorOnlyIfUnique;
import static org.ljc.adoptojdk.converters.MentorFactory.createMentorClassesUsingCSVAsListOfStringsArrays;
import static org.ljc.adoptojdk.converters.MentorFactory.writeBackOutputToFile;

public final class CSVToYAML {

	private static final String NEWLINE_CHAR = "\n";
	private static final String YAML_FILENAME_WITH_LOCATION = "src/main/resources/org/ljc/adoptojdk/converters/JavaPackagesCrosstable-FilteredData.yml";
	private static final String ERROR_MESSAGE = "Error due to: %s%n";
	private static final String SAMPLE_FILE_YML = "src/main/resources/org/ljc/adoptojdk/converters/sampleFile.yml";
	private static final String COLON_WITH_SPACE_SEPARATOR = ": ";
    private static final String LIST_OF_REPLACEMENTS[][] = new String[][] {
            {"!models.Mentors", "mentors:"}, {"!models.Interests", ""}, {"!", "!!"}};
    private static final String EXCLAMATION_MARK = "!";

    private CSVToYAML() {
    }

    public static void main(String[] args) {
		CSVFile csvFileJavaPackages = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/JavaPackagesCrosstable-FilteredData.csv",
				COMMA_SEPARATOR);

		try {
			List<String[]> csvLinesRead = csvFileJavaPackages.readCSVfile();

			Mentors mentors = new Mentors();
			Mentor mentor;
            int idCtr = 1;
			for (int recordIndex = 0; recordIndex < csvLinesRead.size(); recordIndex++) {
				mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
						csvLinesRead, recordIndex, idCtr++);
				mentors = addMentorOnlyIfUnique(mentors, mentor);
			}

			String actualYAMLText = CSVToYAML.convertUsing(Mentors.class,
					mentors, YAML_FILENAME_WITH_LOCATION, LIST_OF_REPLACEMENTS);

			System.out.format("%s", actualYAMLText);
            writeBackOutputToFile(actualYAMLText, YAML_FILENAME_WITH_LOCATION);
		} catch (IOException e) {
			System.out.format(ERROR_MESSAGE, e.getMessage());
		}
    }

	public static String convertUsing(String className, String csvHeaders,
			String csvFieldValues, String[][] listOfReplacements) throws IOException {
		String[] headers = csvHeaders.split(String.valueOf(COMMA_SEPARATOR));
		String[] fieldValues = csvFieldValues.split(String
				.valueOf(COMMA_SEPARATOR));
		String result = EXCLAMATION_MARK + className + NEWLINE_CHAR;
		String eachHeader;
		String eachFieldValue;
		for (int indexCtr = 0; indexCtr < headers.length; indexCtr++) {

			eachHeader = "";
			if (indexCtr < headers.length) {
				eachHeader = headers[indexCtr];
			}

			eachFieldValue = "";
			if (indexCtr < fieldValues.length) {
				eachFieldValue = fieldValues[indexCtr];
			}
			result = result + eachHeader + COLON_WITH_SPACE_SEPARATOR
					+ eachFieldValue + NEWLINE_CHAR;
		}

		return applyCustomisationYamlText(result, listOfReplacements);
	}

	public static <T> String convertUsing(Class<T> clazz, Object someClass,
			String yamlFilename, String[][] listOfReplacements) throws IOException {
		String localYamlFilename = yamlFilename;
		if ((localYamlFilename == null) || localYamlFilename.trim().isEmpty()) {
			localYamlFilename = SAMPLE_FILE_YML;
		}

		YamlWriter writer = new YamlWriter(new FileWriter(localYamlFilename));
		try {
			writer.write(clazz.cast(someClass));
		} finally {
			writer.close();
		}

		return applyCustomisationYamlText(readFromFile(localYamlFilename), listOfReplacements);
	}

	private static String applyCustomisationYamlText(String yamlText, String[][] listOfReplacements) {
        String result = yamlText;
        if (listOfReplacements == null) {
            return result;
        }

        for (String[] eachToken: listOfReplacements) {
            result = result.replaceAll(eachToken[0], eachToken[1]);
        }

		return result;
	}
}
