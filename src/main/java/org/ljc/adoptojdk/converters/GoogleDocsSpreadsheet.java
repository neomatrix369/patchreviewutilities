package org.ljc.adoptojdk.converters;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.util.AuthenticationException;

public final class GoogleDocsSpreadsheet {
	private static final String ERROR_DUE_TO_MSG = "Error due to: %s";

	private GoogleDocsSpreadsheet() {		
	}
	
	/** Our view of Google Spreadsheets as an authenticated Google user. */
	private static SpreadsheetService service = new SpreadsheetService(
			"GoogleDocsSpreadsheet");

	public static Map.Entry<String, String> getCellContent(String cellRow,
			String cellCol) throws AuthenticationException {
		Map<String, String> result = new HashMap<String, String>();
		Properties prop = new Properties();

		FileInputStream propfile;
		try {
			propfile = new FileInputStream(
					"resources/org/ljc/adoptojdk/build.properties");
			prop.load(propfile);

			String username = prop.getProperty("username");
			String password = prop.getProperty("password");
			login(username, password);

			result.put("", "");

		} catch (FileNotFoundException e) {
			System.out.format(ERROR_DUE_TO_MSG, e.getMessage());
		} catch (IOException e) {
			System.out.format(ERROR_DUE_TO_MSG, e.getMessage());
		}

		return result.entrySet().iterator().next();
	}

	public static Boolean login(String username, String password)
			throws AuthenticationException {
		try {
			service.setUserCredentials(username, password);
			return true;
		} catch (Exception ex) {
			throw ex;
		}
	}

	public static Map<String, String> getUserAuthDetails() {
		Map<String, String> userAuthDetails = new HashMap<String, String>();
		Properties prop = new Properties();

		FileInputStream propfile;
		try {
			propfile = new FileInputStream(
					"resources/org/ljc/adoptojdk/build.properties");
			prop.load(propfile);

			String username = prop.getProperty("username");
			String password = prop.getProperty("password");
			
			userAuthDetails.put("username", username);
			userAuthDetails.put("password", password);
		} catch (FileNotFoundException e) {
			System.out.format(ERROR_DUE_TO_MSG, e.getMessage());
		} catch (IOException e) {
			System.out.format(ERROR_DUE_TO_MSG, e.getMessage());
		}
		
		return userAuthDetails;
	}
}
