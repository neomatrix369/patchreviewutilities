package org.ljc.adoptojdk.converters;

import models.Interest;
import models.Mentor;
import models.Mentors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.ljc.adoptojdk.converters.MentorFactory.addMentorOnlyIfUnique;
import static org.ljc.adoptojdk.converters.MentorFactory.createMentorClassesUsingCSVAsListOfStringsArrays;
import static org.ljc.adoptojdk.converters.MentorFactory.writeBackOutputToFile;

public final class CSVToSQL {
    public static final String MENTOR_TABLE_FIELDS = "id,name,email,mentor_type";

    private static final String NEW_LINEFEED = "\n";
    private static final String COLON_SEPARATOR = ":";
    private static final String INSERT_INTO_MENTORS_INTERESTS_TABLE_TEMPLATE = "insert into %s values(%s,%s);\n";
    private static final String INSERT_INTO_MENTORS_TABLE_TEMPLATE = "insert into %s (%s) values(%s,%s,%s,%s);\n";
    private static final String INSERT_INTO_INTERESTS_TABLE_TEMPLATE = "insert into %s values(%s,%s,%s);\n";
    private static final String COMMA_SEPARATOR = ",";
    private static final String EQUAL_TO_SEPARATOR = "=";
    private static final boolean SKIP_NULL_VALUE_FIELDS = true;
    private static final int ID_MENTORS_TABLE = 0;
    private static final int ID_INTERESTS_TABLE = 1;
    private static final int ID_MENTORS_INTERESTS_TABLE = 2;
    private static final String MENTORS_TABLE = "mentor";
    private static final String INTERESTS_TABLE = "interest";
    private static final String MENTORS_INTERESTS_TABLE = "mentor_interest";

    private static final int INTEREST_TABLE_FIELD_PATH = 0;
    private static final int INTEREST_TABLE_FIELD_PROJECT = 1;

    private static final int MENTORS_TABLE_FIELD_NAME = 2;
    private static final int MENTORS_TABLE_FIELD_EMAIL = 3;
    private static final int MENTORS_TABLE_FIELD_MENTOR_TYPE = 4;
    private static final String ID_ONE = "1";
    private static final int NOT_FOUND = -1;
    private static final String SQL_FILENAME_WITH_LOCATION = "src/main/resources/org/ljc/adoptojdk/converters/JavaPackagesCrosstable-FilteredData.sql";

    public static void main(String[] args) throws IOException {
        CSVFile csvFileJavaPackages = new CSVFile(
                "src/main/resources/org/ljc/adoptojdk/converters/JavaPackagesCrosstable-FilteredData.csv",
                COMMA_SEPARATOR);

        try {
            List<String[]> csvLinesRead = csvFileJavaPackages.readCSVfile();

            Mentors mentors = new Mentors();
            Mentor mentor;
            int idCtr = 1;
            for (int recordIndex = 0; recordIndex < csvLinesRead.size(); recordIndex++) {
                mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
                        csvLinesRead, recordIndex, idCtr++);
                mentors = addMentorOnlyIfUnique(mentors, mentor);
            }

            String SQLText = CSVToSQL.convertUsing(mentors);
            SQLText = addAdditionalCustomisationTo(SQLText);
            System.out.println(SQLText);

            writeBackOutputToFile(SQLText, SQL_FILENAME_WITH_LOCATION);

        } catch (IOException e) {
            throw e;
        }
    }

    private static String addAdditionalCustomisationTo(String SQLText) {
        SQLText = "# --- !Ups\n\n" + SQLText;
        return SQLText;
    }

    private CSVToSQL() {
        /*
          Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
         */
    }

    public static String convertUsing(String tableName, String csvText) {
        return String.format("insert into %s values (%s);", tableName, csvText);
    }

    public static String convertUsing(String tableName, Mentors<Mentor> mentors) {
        String commaSeparatedValues, sqlStatementsForAllMentors = "";
        for (Mentor mentor: mentors) {
            commaSeparatedValues = extractFieldValuesFromTheMentor(mentor, SKIP_NULL_VALUE_FIELDS);
            String sqlForEachLine = convertUsing(tableName, commaSeparatedValues);

            sqlStatementsForAllMentors = sqlStatementsForAllMentors + sqlForEachLine;

            if (mentors.size() > 1) {
                sqlStatementsForAllMentors = sqlStatementsForAllMentors + NEW_LINEFEED;
            }
        }
        return sqlStatementsForAllMentors;
    }

    private static String extractFieldValuesFromTheMentor(Mentor mentor, boolean skipNullValueFields) {
        String mentorAsAString = mentor.toString();
        String fieldValuesPairPartOfTheMentorString = mentorAsAString.split(COLON_SEPARATOR)[1];
        String[] fieldValuesPairsInTheMentorString = fieldValuesPairPartOfTheMentorString.split(COMMA_SEPARATOR);
        return extractFieldValuesFromStringArray(fieldValuesPairsInTheMentorString, skipNullValueFields);
    }

    private static String extractFieldValuesFromStringArray(String[] fieldValuesPairsInTheMentorString, boolean skipNullValueFields) {
        String allFieldValues = "";
        for (String eachFieldValuePair: fieldValuesPairsInTheMentorString) {
            String eachFieldValue = eachFieldValuePair.split(EQUAL_TO_SEPARATOR)[1];

            if (theFieldValueIsNull(skipNullValueFields, eachFieldValue)) {
                continue;
            }

            eachFieldValue = surroundValueWithSingleQuotes(eachFieldValue);
            if (theFieldValueIsEmpty(allFieldValues)) {
                allFieldValues = eachFieldValue;
            } else {
                allFieldValues = getCommaSeparatedFieldValuesString(allFieldValues, eachFieldValue);
            }
        }
        return allFieldValues;
    }

    private static String getCommaSeparatedFieldValuesString(String allFieldValues, String eachFieldValue) {
        return allFieldValues + COMMA_SEPARATOR + eachFieldValue;
    }

    private static boolean theFieldValueIsNull(boolean skipNullValueFields, String eachFieldValue) {
        return skipNullValueFields && (eachFieldValue.equals("null"));
    }

    private static boolean theFieldValueIsEmpty(String allFieldValues) {
        return "".equals(allFieldValues);
    }

    private static String surroundValueWithSingleQuotes(String eachFieldValue) {
        return String.format("'%s'", eachFieldValue);
    }

    public static String convertUsing(String[] tableNames, String csvText) {
        String[] multipleCSVLines = csvText.split(NEW_LINEFEED);
        if (multipleCSVLines.length == 0) {
            return "";
        }

        String[] csvTokens = multipleCSVLines[0].split(COMMA_SEPARATOR);

        String interestsSQLLine = String.format(
                INSERT_INTO_INTERESTS_TABLE_TEMPLATE,
                tableNames[ID_INTERESTS_TABLE],
                ID_ONE,
                csvTokens[INTEREST_TABLE_FIELD_PATH],
                csvTokens[INTEREST_TABLE_FIELD_PROJECT]);

        String mentorsSQLLine = String.format(
                INSERT_INTO_MENTORS_TABLE_TEMPLATE,
                tableNames[ID_MENTORS_TABLE],
                MENTOR_TABLE_FIELDS,
                ID_ONE,
                csvTokens[MENTORS_TABLE_FIELD_NAME],
                csvTokens[MENTORS_TABLE_FIELD_EMAIL],
                csvTokens[MENTORS_TABLE_FIELD_MENTOR_TYPE]);

        String mentorInterestsSQLLine = String.format(
                INSERT_INTO_MENTORS_INTERESTS_TABLE_TEMPLATE,
                tableNames[ID_MENTORS_INTERESTS_TABLE],
                ID_ONE,
                ID_ONE);

        return mentorsSQLLine + interestsSQLLine + mentorInterestsSQLLine;
    }

    public static String convertUsing(Mentors<Mentor> mentors) {
        if ((mentors == null) || (mentors.size() == 0)) {
            return "";
        }

        String mentorSQLLine = "";
        String interestsSQLLine = "";
        String mentorsAndInterestsSQLLine = "";

        mentors = normaliseMentors(mentors);
        mentors = normaliseInterests(mentors);

        mentorSQLLine = prepareMentorsSQLText(mentors, mentorSQLLine);

        List<Interest> masterInterestsList = new ArrayList<>();
        for (Mentor mentor: mentors) {
            for (Interest interest: mentor.interests) {
                if (!foundInterestInList(masterInterestsList, interest)) {
                     /*
                      *  if found do nothing - basically don't add this entry to the list of inserts generated so far,
                      *  otherwise do the below.
                      */
                    interestsSQLLine = prepareInterestsSQLText(interestsSQLLine, interest);
                    masterInterestsList.add(interest);
                }

                mentorsAndInterestsSQLLine = prepareMentorsAndInterestSQLText(mentorsAndInterestsSQLLine, mentor, interest);
            }
        }

        return  mentorSQLLine + interestsSQLLine + mentorsAndInterestsSQLLine;
    }

    private static Mentors<Mentor> normaliseMentors(Mentors<Mentor> mentors) {
        Mentors<Mentor> masterMentorsList = new Mentors<>();

        for (Iterator<Mentor> mentorIterator = mentors.iterator(); mentorIterator.hasNext();) {
            Mentor eachMentor = mentorIterator.next();
            if (eachMentor == null) { // remove null references
                mentorIterator.remove();
            } else { // add a mentor that does not exists to the master list
                masterMentorsList = addMentorsToMasterList(masterMentorsList, eachMentor);
            }
        }

        int mentorIndex = 0;
        for (Mentor mentor: masterMentorsList) {
            mentor.id = ++mentorIndex;
            masterMentorsList.set(mentorIndex - 1, mentor);
        }

        return masterMentorsList;
    }

    private static Mentors<Mentor> addMentorsToMasterList(Mentors<Mentor> masterMentorsList, Mentor mentor) {
        for (Mentor eachMentor: masterMentorsList) {
             if (eachMentor.email.equals(mentor.email)) {
                 return masterMentorsList;
             }
        }

        masterMentorsList.add(mentor);
        return masterMentorsList;
    }

    private static Mentors<Mentor> normaliseInterests(Mentors<Mentor> mentors) {
        List<Interest> masterInterestsList = new ArrayList<>();

        for (Mentor mentor: mentors) {
            masterInterestsList = addInterestsToMasterList(masterInterestsList, mentor.interests);
        }

        int interestIndex = NOT_FOUND;
        for (Interest interest: masterInterestsList) {
            interestIndex++;
            interest.id =  interestIndex + 1;
            masterInterestsList.set(interestIndex, interest);
        }

        int mentorIndex = NOT_FOUND;
        for (Mentor mentor: mentors) {
            mentorIndex++;
            for (Interest interest: mentor.interests) {
                int masterInterestListIndex = fetchIndexOfInterestIfFound(masterInterestsList, interest);
                if (masterInterestListIndex == NOT_FOUND) {
                    // do nothing, keep looping through mentors
                } else {
                    int interestsListIndex = fetchIndexOfInterestIfFound(mentor.interests, interest);
                    interest.id = masterInterestsList.get(masterInterestListIndex).id;
                    mentor.interests.set(interestsListIndex, interest);
                    mentors.set(mentorIndex, mentor);
                }
            }
        }

        return mentors;
    }

    private static String prepareMentorsSQLText(Mentors<Mentor> mentors, String mentorSQLLine) {
        for (Mentor mentor: mentors) {
            String mentorTypeAsString = "";
            if (mentor.mentorType != null) {
                mentorTypeAsString = mentor.mentorType.toString();
            }
            mentorSQLLine = mentorSQLLine + String.format(INSERT_INTO_MENTORS_TABLE_TEMPLATE,
                MENTORS_TABLE,
                MENTOR_TABLE_FIELDS,
                mentor.id,
                surroundValueWithSingleQuotes(mentor.name),
                surroundValueWithSingleQuotes(mentor.email),
                surroundValueWithSingleQuotes(mentorTypeAsString));
        }
        return mentorSQLLine;
    }

    private static boolean foundMentorInMasterList(List<Mentor> masterMentorsList, Mentor mentor) {
        for (Mentor eachMentor: masterMentorsList) {
            if (eachMentor.email.equals(mentor.email)) {
                return true;
            }
        }

        return false;
    }

    private static String prepareInterestsSQLText(String interestsSQLLine, Interest interest) {
        interestsSQLLine = interestsSQLLine + String.format(INSERT_INTO_INTERESTS_TABLE_TEMPLATE,
                INTERESTS_TABLE,
                interest.id,
                surroundValueWithSingleQuotes(interest.path),
                surroundValueWithSingleQuotes(interest.project));
        return interestsSQLLine;
    }

    private static String prepareMentorsAndInterestSQLText(String mentorsAndInterestsSQLLine, Mentor mentor, Interest interest) {
        mentorsAndInterestsSQLLine = mentorsAndInterestsSQLLine +
                String.format(INSERT_INTO_MENTORS_INTERESTS_TABLE_TEMPLATE,
                        MENTORS_INTERESTS_TABLE,
                        mentor.id,
                        interest.id);
        return mentorsAndInterestsSQLLine;
    }

    private static List<Interest> addInterestsToMasterList(List<Interest> masterInterestsList, List<Interest> interests) {

        if ((masterInterestsList == null)) {
            return null;
        }

        if (interests == null) {
            return masterInterestsList;
        }

        for (Interest interest: interests) {
            if (!foundInterestInList(masterInterestsList, interest)) {
                masterInterestsList.add(interest);
            }
        }

        return masterInterestsList;
    }

    private static boolean foundInterestInList(List<Interest> masterInterestsList, Interest interest) {
        int index = fetchIndexOfInterestIfFound(masterInterestsList, interest);
        return index != NOT_FOUND;
    }

    private static int fetchIndexOfInterestIfFound(List<Interest> masterInterestsList, Interest interest) {
        int index = NOT_FOUND;
        for (Interest eachInterest: masterInterestsList) {
            index++;
            if (interest.project.equals(eachInterest.project)) {
                return index;
            }
        }
        return NOT_FOUND;
    }
}