package org.ljc.adoptojdk.converters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public final class CommonFunctions {
	private CommonFunctions() {		
	}
	
	public static final String NEW_LINE_CHAR = "\n";
	public static final String ESCAPED_SINGLE_QUOTE = "\"";	
	public static final String REGEX_FOR_ONE_OR_MORE_WORDS = "\b?(\\w?)";
	public static final String COMMA_SEPARATOR = ",";

	public static boolean hasMoreThanOneWord(String oneOrMoreWords) {
		String[] words = oneOrMoreWords.split(REGEX_FOR_ONE_OR_MORE_WORDS);
		return (words.length > 0);
	}
	
	public static String readFromFile(String fileName) {
		StringBuilder fileContent = new StringBuilder();
		File file = new File(fileName);		
		FileReader fr = null;
		try {
			fr = new FileReader(file);
			internalBufferedReading(fileContent, fr);
			
		} catch (FileNotFoundException e) {
			System.out.format("Error: %s", e.getMessage());
		} catch (IOException e) {
			System.out.format("Error: %s", e.getMessage());
		} finally {
			try {
				fr.close();
			} catch (IOException e) {
				System.out.format("%s", e.getMessage());
			}
		}
		return fileContent.toString();
	}

	private static void internalBufferedReading(StringBuilder fileContent,
			FileReader fr) throws IOException {
		BufferedReader br = new BufferedReader(fr);
		try {
			String eachLine;
			while ((eachLine = br.readLine()) != null) {
				fileContent.append(eachLine);
				fileContent.append("\n");
			}
		} finally {
			br.close();
		}
	}
}