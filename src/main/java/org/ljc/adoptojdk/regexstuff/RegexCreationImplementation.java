package org.ljc.adoptojdk.regexstuff;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.ljc.adoptojdk.*;

public final class RegexCreationImplementation {
	private static final int PACKAGE_UPTO_LEVEL_4 = 4;
	private static final int PACKAGE_UPTO_LEVEL_2 = 2;
	private static final int PACKAGE_UPTO_LEVEL_3 = 3;
	private static final String CLASSNAME_METHODNAME_LINENUMBER_MSG = "Classname: %s Methodname: %s Linenumber %d %n";
	private static final String ERROR_DUE_TO_MSG = "Error due to: %s %n";

	private RegexCreationImplementation() {
	}
	
	public static void main(String[] args) throws IOException {
		String parameters = Arrays.toString(args).toLowerCase();
		if (parameters.contains("-corba")) {
			displayPackageNamesForRepoCorbaOnConsole(PACKAGE_UPTO_LEVEL_2);
		} else if (parameters.contains("-hotspot")) {
			displayPackageNamesForRepoHotspotOnConsole(PACKAGE_UPTO_LEVEL_2);
		} else if (parameters.contains("-jaxp")) {
			displayPackageNamesForRepoJaxpOnConsole(PACKAGE_UPTO_LEVEL_3);
		} else if (parameters.contains("-jaxws")) {
			displayPackageNamesForRepoJaxpOnConsole(PACKAGE_UPTO_LEVEL_3);
		} else if (parameters.contains("-jdk")) {
			displayPackageNamesForRepoJdkOnConsole(PACKAGE_UPTO_LEVEL_3);
		} else if (parameters.contains("-langtools")) {
			displayPackageNamesForRepoLangtoolsOnConsole(PACKAGE_UPTO_LEVEL_3);
		} else if (parameters.contains("-nashorn")) {
			displayPackageNamesForRepoNashornOnConsole(PACKAGE_UPTO_LEVEL_4);
		}
	}

	private static void displayPackageNamesForRepoNashornOnConsole(int packageLevel) throws IOException {
		String nashornListFileName = "src/main/resources/org/ljc/adoptojdk/nashornRepoFilesAndFoldersList.txt ";
		String nashornPrefixesListFilename = "src/main/resources/org/ljc/adoptojdk/nashornRepoPrefixesList.txt";
		
		List<String> listOfPaths = readFileIntoArrayList(nashornListFileName);
		List<String> listOfPrefixesToStrip = readFileIntoArrayList(nashornPrefixesListFilename);		                        

		displayPackageNamesToConsole(listOfPaths, listOfPrefixesToStrip, packageLevel);
	}
	
	private static void displayPackageNamesForRepoLangtoolsOnConsole(int packageLevel) throws IOException {
		String langtoolsListFileName = "src/main/resources/org/ljc/adoptojdk/langtoolsRepoFilesAndFoldersList.txt";
		String langtoolsPrefixesListFilename = "src/main/resources/org/ljc/adoptojdk/langtoolsRepoPrefixesList.txt";
		
		List<String> listOfPaths = readFileIntoArrayList(langtoolsListFileName);
		List<String> listOfPrefixesToStrip = readFileIntoArrayList(langtoolsPrefixesListFilename);		                        

		displayPackageNamesToConsole(listOfPaths, listOfPrefixesToStrip, packageLevel);
	}

	private static void displayPackageNamesForRepoJdkOnConsole(int packageLevel) throws IOException {
		String jdkListFileName = "src/main/resources/org/ljc/adoptojdk/jdkRepoFilesAndFoldersList.txt";
		String jdkPrefixesListFilename = "src/main/resources/org/ljc/adoptojdk/jdkRepoPrefixesList.txt";
		
		List<String> listOfPaths = readFileIntoArrayList(jdkListFileName);
		List<String> listOfPrefixesToStrip = readFileIntoArrayList(jdkPrefixesListFilename);		                        

		displayPackageNamesToConsole(listOfPaths, listOfPrefixesToStrip, packageLevel);
	}

	private static void displayPackageNamesForRepoJaxpOnConsole(int packageLevel) throws IOException {
		String jaxpListFileName = "src/main/resources/org/ljc/adoptojdk/jaxpRepoFilesAndFoldersList.txt";
		String jaxpPrefixesListFilename = "src/main/resources/org/ljc/adoptojdk/jaxpRepoPrefixesList.txt";
		
		List<String> listOfPaths = readFileIntoArrayList(jaxpListFileName);
		List<String> listOfPrefixesToStrip = readFileIntoArrayList(jaxpPrefixesListFilename);		                        

		displayPackageNamesToConsole(listOfPaths, listOfPrefixesToStrip, packageLevel);
	}
	
	private static void displayPackageNamesForRepoCorbaOnConsole(int packageLevel) throws IOException {
		String corbaListFileName = "src/main/resources/org/ljc/adoptojdk/corbaRepoFilesAndFoldersList.txt";
		String corbaPrefixesListFilename = "src/main/resources/org/ljc/adoptojdk/corbaRepoPrefixesList.txt";
		
		List<String> listOfPaths = readFileIntoArrayList(corbaListFileName);
		List<String> listOfPrefixesToStrip = readFileIntoArrayList(corbaPrefixesListFilename);		                        

		displayPackageNamesToConsole(listOfPaths, listOfPrefixesToStrip, packageLevel);
	}
	
	private static void displayPackageNamesForRepoHotspotOnConsole(int packageLevel) throws IOException {
		String hotspotListFilename = "src/main/resources/org/ljc/adoptojdk/hotspotRepoFullFilesDirStructure.txt";
		String hotspotPrefixListFilename = "src/main/resources/org/ljc/adoptojdk/hotspotRepoPrefixesList.txt";
		
		List<String> listOfPaths = readFileIntoArrayList(hotspotListFilename);		
		List<String> listOfPrefixesToStrip = readFileIntoArrayList(hotspotPrefixListFilename);
		
		displayPackageNamesToConsole(listOfPaths, listOfPrefixesToStrip, packageLevel);
	}

	private static void displayPackageNamesToConsole(List<String> listOfPaths,
			List<String> listOfPrefixesToStrip, int packageLevel) {
		List<String> listOfPackageNames = new ArrayList<>();
		RegexCreationEngine regexEngine = new RegexCreationEngine();
		listOfPackageNames = regexEngine
				.retrievePackageNamesFromListOfPaths(listOfPaths,
						listOfPrefixesToStrip, packageLevel);
		for (String eachPackagename: listOfPackageNames) {
			System.out.format("%s%n", eachPackagename);			
		}
	}
		
	private static List<String> readFileIntoArrayList(String filename) {
		List<String> listOfPaths = new ArrayList<String>();
		
		BufferedReader bufferedReader;
		try {
			bufferedReader = new BufferedReader(new FileReader(
					filename));
			String eachLine;

			try {
					while ((eachLine = bufferedReader.readLine()) != null) {
						listOfPaths.add(eachLine);
					}
			} catch (Exception e) {
				showErrorMessageInConsole(e);
			} finally {
				bufferedReader.close();
			}
		} catch (IOException e) {
			showErrorMessageInConsole(e);
		} 
		
		return listOfPaths;
	}

	private static void showErrorMessageInConsole(Exception e) {
		System.out.format(ERROR_DUE_TO_MSG, e.getMessage());
		System.out.format(CLASSNAME_METHODNAME_LINENUMBER_MSG,
				e.getStackTrace()[0].getClassName(),
				e.getStackTrace()[0].getMethodName(),
				e.getStackTrace()[0].getLineNumber());
	}
}