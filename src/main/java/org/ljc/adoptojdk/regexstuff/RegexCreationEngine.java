package org.ljc.adoptojdk.regexstuff;

import static org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants.*;
import static org.ljc.adoptojdk.DirectoryFileUtility.*;

import java.util.ArrayList;
import java.util.List;

public final class RegexCreationEngine {
	private static final String STAR_ALL = "*";
	private static final int FULL_PACKAGE_LEVELS = -1;
	private static final String TWO_STRING_FORMAT_TERMS = "%s%s";
	private String directorySeparator = DEFAULT_DIRECTORY_SEPARATOR;
	private String packageNameSeparator = DEFAULT_PACKAGENAME_SEPARATOR;

	public RegexCreationEngine() {
		this(DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
	}

	public RegexCreationEngine(String directorySeparator) {
		this(directorySeparator, DEFAULT_PACKAGENAME_SEPARATOR);
	}

	public RegexCreationEngine(String directorySeparator,
			String packageNameSeparator) {
		this.directorySeparator = directorySeparator;
		this.packageNameSeparator = packageNameSeparator;
	}

	public String createRegexFromPath(String inDirectoryOrFilename) {
		String inNativeDirectoryOrFilename = inDirectoryOrFilename;

		if (inNativeDirectoryOrFilename.trim().isEmpty()) {
			return inNativeDirectoryOrFilename;
		}

		if (isInvalidPath(inNativeDirectoryOrFilename)) {
			return inNativeDirectoryOrFilename;
		}

		if (hasTrailingSeparator(inNativeDirectoryOrFilename,
				directorySeparator)) {
			inNativeDirectoryOrFilename = dropTrailingSeparator(
					inNativeDirectoryOrFilename, directorySeparator);
		} else {
			String inPathWithoutFilename = removeFilenameFromPath(inNativeDirectoryOrFilename, directorySeparator);
			if (filenameHasBeenRemoved(inPathWithoutFilename,
					inNativeDirectoryOrFilename)) {
				return String.format(TWO_STRING_FORMAT_TERMS,
						inPathWithoutFilename, ONE_WORD_CHAR_REGEX);
			}
		}

		return createRegexFromDirectory(inNativeDirectoryOrFilename);
	}

	private String createRegexFromDirectory(String inNativeDirectoryOrFilename) {
		String[] pathSplitIntoAnArray = inNativeDirectoryOrFilename
				.split(directorySeparator);
		int numberOfLevels = pathSplitIntoAnArray.length;
		int firstElementIndex = 0;
		int lastElementIndex = numberOfLevels - 1;

		switch (numberOfLevels) {
		case DIRECTORY_ZEROTH_LEVEL: {
			return inNativeDirectoryOrFilename;
		}

		case DIRECTORY_FIRST_LEVEL: {
			return regexForASingleDirectoryOrFilename(inNativeDirectoryOrFilename);
		}

		case DIRECTORY_SECOND_LEVEL: {
			return regexForADirectoryUptoTwoLevels(pathSplitIntoAnArray,
					firstElementIndex);
		}

		case DIRECTORY_THIRD_LEVEL: {
			return regexForADirectoryUptoThreeLevels(pathSplitIntoAnArray,
					firstElementIndex, lastElementIndex);
		}

		// four or more level
		default: {
			return regexForADirectoryUptoFourOrMoreLevels(pathSplitIntoAnArray,
					firstElementIndex, lastElementIndex);
		}
		}
	}

	private String regexForADirectoryUptoFourOrMoreLevels(
			String[] pathSplitIntoAnArray, int firstElementIndex,
			int lastElementIndex) {
		return String.format("%s%s%s%s%s%s",
				pathSplitIntoAnArray[firstElementIndex], directorySeparator,
				ONE_OR_MORE_WORD_CHARS_REGEX, directorySeparator,
				pathSplitIntoAnArray[lastElementIndex],
				ZERO_OR_ONE_DIR_SEPARATOR_REGEX);
	}

	private String regexForADirectoryUptoThreeLevels(
			String[] pathSplitIntoAnArray, int firstElementIndex,
			int lastElementIndex) {
		return String.format("%s%s%s%s%s%s",
				pathSplitIntoAnArray[firstElementIndex], directorySeparator,
				ONE_WORD_CHAR_REGEX, directorySeparator,
				pathSplitIntoAnArray[lastElementIndex],
				ZERO_OR_ONE_DIR_SEPARATOR_REGEX);
	}

	private String regexForADirectoryUptoTwoLevels(
			String[] pathSplitIntoAnArray, int firstElementIndex) {
		return String.format("%s%s%s%s",
				pathSplitIntoAnArray[firstElementIndex], directorySeparator,
				ZERO_OR_ONE_WORD_CHAR_REGEX, ZERO_OR_ONE_DIR_SEPARATOR_REGEX);
	}

	private String regexForASingleDirectoryOrFilename(
			String inNativeDirectoryOrFilename) {
		return String.format(TWO_STRING_FORMAT_TERMS,
				inNativeDirectoryOrFilename, ZERO_OR_ONE_DIR_SEPARATOR_REGEX);
	}

	public String retrievePackageNameFromPath(String inputPath,
			List<String> prefixesToStrip) {
		return retrievePackageNameFromPath(inputPath, prefixesToStrip, FULL_PACKAGE_LEVELS);
	}

	public String retrievePackageNameFromPath(String inputPath,
			List<String> inPrefixesToUse, int inPackageLevel) {
		String localInputPath = inputPath;
		
		List<String> prefixesToUse = sortGiven(inPrefixesToUse, inDescendingOrderByLength());
		
		String replacedInputPath = localInputPath;
		for (String eachPrefixToUse : prefixesToUse) {
			if (replacedInputPath.contains(eachPrefixToUse)) {
				replacedInputPath = replacedInputPath.replace(
						eachPrefixToUse, "");
				
				if (hasTrailingSeparator(replacedInputPath, directorySeparator)) {
					replacedInputPath = dropTrailingSeparator(replacedInputPath,
							directorySeparator);
				} else {
					replacedInputPath = dropTrailingSeparator(
							removeFilenameFromPath(replacedInputPath, directorySeparator), directorySeparator);
				}
				
				replacedInputPath = replacedInputPath.replaceAll(directorySeparator,
						packageNameSeparator);
				break;
			}
		}

		int packageLevel = inPackageLevel;
		if (packageLevel == FULL_PACKAGE_LEVELS) {
			return replacedInputPath;
		}
		
		String[] splitPackagename = replacedInputPath.split(ESCAPE_TOKEN + packageNameSeparator);		
		if (splitPackagename.length < packageLevel) {
			packageLevel = splitPackagename.length;  
		}
		
		if (splitPackagename.length > 0) {
			replacedInputPath = "";
			int levelCtr=0;
			do  {			
				if (!splitPackagename[levelCtr].isEmpty()) {
					replacedInputPath += splitPackagename[levelCtr] + packageNameSeparator;
				}
				levelCtr++;
			}  while (levelCtr < packageLevel);
			
			if (!replacedInputPath.isEmpty()) {
				replacedInputPath += STAR_ALL;
			}
		}
		return replacedInputPath;
	}
	
	
	public String retrieveRegexContainingPackageNameFromPath(String inputPath,
			List<String> inPrefixesToStrip) {
		String localInputPath = inputPath;
		
		List <String> prefixesToStrip = sortGiven(inPrefixesToStrip, inDescendingOrderByLength());
		
		if (hasTrailingSeparator(localInputPath, directorySeparator)) {
			localInputPath = dropTrailingSeparator(inputPath,
					directorySeparator);
			localInputPath = String.format(TWO_STRING_FORMAT_TERMS,
					localInputPath, ZERO_OR_ONE_DIR_SEPARATOR_REGEX);
		} else {
			localInputPath = removeFilenameFromPath(localInputPath, directorySeparator);
			localInputPath = String.format(TWO_STRING_FORMAT_TERMS,
					localInputPath, ONE_WORD_CHAR_REGEX);
		}

		for (String eachPrefixToUse : prefixesToStrip) {
			if (localInputPath.contains(eachPrefixToUse)) {
				if (hasTrailingSeparator(eachPrefixToUse, directorySeparator)) {
					return localInputPath.replace(eachPrefixToUse,
							ONE_OR_MORE_WORD_CHARS_REGEX + directorySeparator);
				} else {
					return localInputPath.replace(eachPrefixToUse,
							ONE_OR_MORE_WORD_CHARS_REGEX);
				}
			}
		}
		return localInputPath;
	}

	public List<String> retrievePackageNamesFromListOfPaths(
			List<String> listOfInputPaths, List<String> listOfPrefixesToStrip) {
		return retrievePackageNamesFromListOfPaths(listOfInputPaths, listOfPrefixesToStrip, FULL_PACKAGE_LEVELS);
	}

	public List<String> retrievePackageNamesFromListOfPaths(
			List<String> listOfPaths, List<String> listOfPrefixesToStrip, int packageLevels) {
		List<String> packageNames = new ArrayList<String>();

		for (String eachInputPath : listOfPaths) {
			String eachPackageName = retrievePackageNameFromPath(eachInputPath,
					listOfPrefixesToStrip, packageLevels);
			
			if (!eachPackageName.isEmpty()) {
				packageNames.add(eachPackageName);
			}
		}
		
		packageNames = removeDuplicateItems(packageNames);
		packageNames = sortGiven(packageNames, inAscendingOrderByName());
		
		return packageNames;
	}
	
	
	public List<String> retrieveRegexContainingPackageNameFromListOfPath(
			List<String> listOfInputPaths, List<String> inPrefixesToStrip) {
		List<String> packageNames = new ArrayList<String>();
		
		List<String> prefixesToStrip = sortGiven(inPrefixesToStrip, inDescendingOrderByLength());
		
		for (String eachInputPath : listOfInputPaths) {
			String eachPackageName = retrieveRegexContainingPackageNameFromPath(eachInputPath,
					prefixesToStrip);
			if (!eachPackageName.isEmpty()) {
				packageNames.add(eachPackageName);
			}
		}
		return packageNames;
	}
}