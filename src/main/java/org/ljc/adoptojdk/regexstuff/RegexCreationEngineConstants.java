package org.ljc.adoptojdk.regexstuff;

public final class RegexCreationEngineConstants {
	public static final int DIRECTORY_ZEROTH_LEVEL = 0;
	public static final int DIRECTORY_FIRST_LEVEL = 1;
	public static final int DIRECTORY_SECOND_LEVEL = 2;
	public static final int DIRECTORY_THIRD_LEVEL = 3;

	public static final String DEFAULT_DIRECTORY_SEPARATOR = "/";
	public static final String DEFAULT_PACKAGENAME_SEPARATOR = ".";
	public static final String ESCAPE_TOKEN = "\\";
	public static final String ONE_WORD_CHAR_REGEX = "(\\w)";	
	public static final String ONE_OR_MORE_WORD_CHARS_REGEX = "(\\w+)";
	public static final String ZERO_OR_ONE_WORD_CHAR_REGEX = "(\\w?)";
	public static final String ZERO_OR_MORE_WORD_CHARS_REGEX = "(\\w*)";
	
	public static final String ANY_NUMBER_OF_CHARS_REGEX = "(.*)";
	
	public static final String ZERO_OR_ONE_DIR_SEPARATOR_REGEX = String
			.format("(%s?)", DEFAULT_DIRECTORY_SEPARATOR);

	public static final char[] ILLEGAL_CHARACTERS = { /* '/', */'\n', '\r',
			'\t', '\0', '\f', '`', '?', '*', '\\', '<', '>', '|', '\"', ':' };
	
	private RegexCreationEngineConstants() {
	}
}