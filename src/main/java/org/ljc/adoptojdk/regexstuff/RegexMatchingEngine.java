package org.ljc.adoptojdk.regexstuff;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public final class RegexMatchingEngine {
	private RegexMatchingEngine() {		
	}
	
	public static Matcher regexPatternMatchesText(
				String regexPattern, String textStringToSearch) {
        Pattern pattern = Pattern.compile(regexPattern);
        return pattern.matcher(textStringToSearch);        
	}
}