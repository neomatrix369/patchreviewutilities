package models;

public enum MentorType {
	PROJECT("PROJECT"), LIST("LIST"), GROUP("GROUP");

	private String stringValue;

	MentorType(String stringValue) {
		this.stringValue = stringValue;
	}

	@Override
	public String toString() {
		return stringValue;
	}

	public static MentorType fromString(String text) {
		if (text != null) {
			for (MentorType mentorType : MentorType.values()) {
				if (text.equalsIgnoreCase(mentorType.toString())) {
					return mentorType;
				}
			}
		}
		return MentorType.values()[0];
	}
}