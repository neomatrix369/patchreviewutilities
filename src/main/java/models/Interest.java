package models;

public class Interest {
    public int id;
	public String path;
	public String project;

	@Override
	public String toString() {
		return String.format("Interest: id=%d, path=%s, project=%s", id, path, project);
	}
}
