package models;

import java.util.List;

public class Mentor {
    public int id;
	public String name;
	public String email;
	public MentorType mentorType;
	public List<Interest> interests;

	@Override
	public String toString() {
		return String.format(
				"Mentor: id=%d, name=%s, email=%s, mentorType=%s, interests=%s", id, name, email, mentorType, interests);
	}
}
