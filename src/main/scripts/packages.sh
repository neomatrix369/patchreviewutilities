#!/bin/bash

jdk_folder=~/w/openjdk/src/jdk8_tl

modules=( "corba" "hotspot" "jaxp" "jaxws" "jdk" "langtools" "nashorn" )

for module in "${modules[@]}"
do
    find $jdk_folder/$module -name *.java | xargs grep -m 1 -h "^\s*package .*;" | sed 's|package\s*\(.*\);.*|\1|' | sort | uniq > _packages.$module.txt
done
