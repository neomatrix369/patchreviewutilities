package org.ljc.adoptojdk.regexstuff;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.*;
import java.util.regex.Matcher;
import org.junit.Test;
import org.ljc.adoptojdk.regexstuff.RegexMatchingEngine;

// http://www.regular-expressions.info/java.html
// Section: Regular Expressions, Literal Strings and Backslashes

// http://www.regexplanet.com/advanced/java/index.html

public final class RegexMatchingEngineBehaviour {
	private static final boolean SUCCESSFUL = true;

	@Test
	public void shouldReturnFalseWhenAnEmptyRegexPatternIsMatchedWithNoText() {
		String regexPattern = "";
		String textString = "";
		Matcher patternMatches = RegexMatchingEngine.regexPatternMatchesText(
				regexPattern, textString);
		assertThat(patternMatches.find(), is(SUCCESSFUL));
	}

	@Test
	public void shouldReturnFalseWhenAnEmptyRegexPatternIsMatchedWithAnyText() {
		String regexPattern = "";
		String textString = "Any Text";
		Matcher patternMatches = RegexMatchingEngine.regexPatternMatchesText(
				regexPattern, textString);
		assertThat(patternMatches.find(), is(SUCCESSFUL));
	}
}
