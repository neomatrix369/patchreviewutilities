package org.ljc.adoptojdk.regexstuff;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Matcher;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public final class RegexMatchingEngineParamBehaviours {
	private static final String EXPECTED_PATTERN_MATCHES_TEXT_MSG = "Expected pattern [%s] to match with text [%s].";
	private static final boolean REGEX_PATTERN_DOES_NOT_MATCH = false;
	private static final boolean REGEX_PATTERN_DOES_MATCH = true;

	private String regexPattern = "";
	private String textString = "";
	boolean successfulOrNot = false;

	public RegexMatchingEngineParamBehaviours(String regexPattern,
			String textString, boolean patternMatchResult) {
		this.regexPattern = regexPattern;
		this.textString = textString;
		this.successfulOrNot = patternMatchResult;
	}

	@Parameters
	public static Collection<Object[]> generatedData() {
		return Arrays
				.asList(new Object[][] {
						// repo matching
						{ "^build", "Any Text", REGEX_PATTERN_DOES_NOT_MATCH },
						{ "^build((\\/)?)", "build", REGEX_PATTERN_DOES_MATCH },
						{ "^build((\\/)?)", "build/abc/pqr",
								REGEX_PATTERN_DOES_MATCH },
						{ "^build((\\/)?)", "abc/build/",
								REGEX_PATTERN_DOES_NOT_MATCH },
						{ "^build((\\/)?)", "abc/build/aaa",
								REGEX_PATTERN_DOES_NOT_MATCH },

						{ "^common((\\/)?)", "common/",
								REGEX_PATTERN_DOES_MATCH },
						{ "^common((\\/)?)", "common", REGEX_PATTERN_DOES_MATCH },

						{ "^test((\\/)?)", "test/", REGEX_PATTERN_DOES_MATCH },
						{ "^test((\\/)?)", "test", REGEX_PATTERN_DOES_MATCH },

						{ "^make((\\/)?)", "make/", REGEX_PATTERN_DOES_MATCH },
						{ "^make((\\/)?)", "make", REGEX_PATTERN_DOES_MATCH },

						{ "^hotspot\\/test((\\/)?)", "hotspot/test/",
								REGEX_PATTERN_DOES_MATCH },
						{ "^hotspot((\\/)?)", "hotspot/",
								REGEX_PATTERN_DOES_MATCH },
						{ "^hotspot((\\/)?)", "hotspot",
								REGEX_PATTERN_DOES_MATCH },

						{ "^corba((\\/)?)", "corba/", REGEX_PATTERN_DOES_MATCH },
						{ "^corba((\\/)?)", "corba", REGEX_PATTERN_DOES_MATCH },

						{ "^jaxp((\\/)?)", "jaxp/", REGEX_PATTERN_DOES_MATCH },
						{ "^jaxp((\\/)?)", "jaxp", REGEX_PATTERN_DOES_MATCH },

						{ "^jaxws((\\/)?)", "jaxws/", REGEX_PATTERN_DOES_MATCH },
						{ "^jaxws((\\/)?)", "jaxws", REGEX_PATTERN_DOES_MATCH },

						{ "^jdk((\\/)?)", "jdk/", REGEX_PATTERN_DOES_MATCH },
						{ "^jdk((\\/)?)", "jdk", REGEX_PATTERN_DOES_MATCH },

						{ "^langtools((\\/)?)", "langtools/",
								REGEX_PATTERN_DOES_MATCH },
						{ "^langtools((\\/)?)", "langtools",
								REGEX_PATTERN_DOES_MATCH },

						{ "^nashorn((\\/)?)", "nashorn/",
								REGEX_PATTERN_DOES_MATCH },
						{ "^nashorn((\\/)?)", "nashorn",
								REGEX_PATTERN_DOES_MATCH },

						// filename matching
						{ "^src/share/com/sun((\\/)?)",
								"src/share/com/sun/abc.java",
								REGEX_PATTERN_DOES_MATCH } });
	}

	@Test
	public void shouldEvaluateRegexPatternWithSomeText() {
		Matcher patternMatches = RegexMatchingEngine.regexPatternMatchesText(
				regexPattern, textString);
		assertThat(String.format(EXPECTED_PATTERN_MATCHES_TEXT_MSG,
				regexPattern, textString/*
										 * , fromPosition, toPosition
										 */), patternMatches.find(),
				is(successfulOrNot));
	}
}