package org.ljc.adoptojdk.regexstuff;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.assertThat;
import static org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants.DEFAULT_DIRECTORY_SEPARATOR;
import static org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants.DEFAULT_PACKAGENAME_SEPARATOR;
import static org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants.ONE_OR_MORE_WORD_CHARS_REGEX;
import static org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants.ONE_WORD_CHAR_REGEX;
import static org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants.ZERO_OR_ONE_DIR_SEPARATOR_REGEX;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

/**
 * <pre>
 * 
 * Java-based
 *  http:www.regular-expressions.info/java.html
 *  Section: Regular Expressions, Literal Strings and Backslashes
 * 
 *  http:www.regexplanet.com/advanced/java/index.html
 * 
 * Non-java based
 *  http:rubular.com/
 * 
 * TODO: [done] Given an empty string return an empty string {@link #should_Return_Nothing_When_An_Empty_String_Is_Passed_In()}
 * TODO: [done] Given an invalid path as input string, return the invalid path {@link #should_Return_Input_String_Back_When_An_Invalid_Path_String_Is_Passed_In()}
 * TODO: [done] Given a path of one level is supplied, shrink it to its minimum regex i.e. src ==&gt; src(/?) {@link #should_Return_A_Regex_With_A_Suffix_When_A_Path_Of_First_Level_Is_Passed_In()}
 * TODO: [done] Given a path of two levels is supplied, shrink it to its minimum regex i.e. src/com ==&gt; src/((\w)?)(/?) {@link #should_Return_A_Regex_With_A_Suffix_When_A_Path_Of_Two_Levels_Are_Passed_In()}
 * TODO: [done] Given a filepath and shrink it to its regex minimum i.e. src/share/classes/com ==&gt; src/((\w)*)/com(/?)  {@link #}
 * TODO: [done] Given a filepath with filename and shrink it to its regex minimum without the filename i.e. src/share/classes/com/SomeFile.java ==&gt; src/((/w)*)/com(/?)   {@link #} 
 * TODO: [done] Given a filepath with no filename, a prefix to strip, should return the contained package name {@link #should_Return_A_PackageName_Given_A_Path_With_No_Filename_And_A_Prefix_To_Strip()}
 * TODO: [done] Given a filepath with filename, a prefix to strip, should return the contained package name {@link #should_Return_A_PackageName_Given_A_Path_With_Filename_And_A_Prefix_To_Strip()}
 * TODO: [done] Given a filepath with no filename, a prefix to strip, should return the regex containing the package name {@link #should_Return_A_Regex_Containing_PackageName_Given_A_Path_With_No_Filename_And_A_Prefix()}
 * TODO: [done] Given a filepath with filename, a prefix to strip, should return the regex containing the package name {@link #should_Return_A_Regex_Containing_PackageName_Given_A_Path_With_Filename_And_A_Prefix()}
 * 
 * 
 * TODO: [done] Given list of a filepaths with filename, one or more prefix to strip, should return a list of regexes containing the package name {@link #}
 * TODO: [done] Given list of a filepaths with filename, one or more prefix to strip, should return a list of regexes containing the package name {@link #}
 * TODO: [done] Given list of a filepaths with no filename, one or more prefix to strip, should return a list of regexes containing the package name {@link #}
 * TODO: [done] Given list of a filepaths with filename, one or more prefix to strip, should return a list of regexes containing the package name {@link #}
 * 
 * TODO: [done] Test the individual Constant expressions {@link AbstractRegexBehaviours}
 * TODO: [underway] Link TODO elements to implemented tests
 * 
 * </pre>
 */

public final class RegexCreationEngineBehaviours {

	@Test
	public void should_Return_Nothing_When_An_Empty_String_Is_Passed_In() {
		String inputPath = "";
		RegexCreationEngine regexEngine = new RegexCreationEngine();
		String regexCreated = regexEngine.createRegexFromPath(inputPath);
		assertThat("An empty input string hasn't returns an empty string.",
				regexCreated.isEmpty(), is(true));
	}

	@Test
	public void should_Return_Input_String_Back_When_An_Invalid_Path_String_Is_Passed_In() {
		String inputPath = "zdsdfzz\\srgdfg/sefsf";
		RegexCreationEngine regexEngine = new RegexCreationEngine();
		String regexCreated = regexEngine.createRegexFromPath(inputPath);
		assertThat(
				"The invalid input string has not been returned as regex string.",
				regexCreated, is(inputPath));
	}

	@Test
	public void should_Return_A_Regex_With_A_Suffix_When_A_Path_Of_First_Level_Is_Passed_In() {
		String inputPath = "src/";
		String expectedRegex = String.format("src%s",
				ZERO_OR_ONE_DIR_SEPARATOR_REGEX);
		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR);
		String regexCreated = regexEngine.createRegexFromPath(inputPath);
		assertThat(
				String.format(
						"Regex did not create the expected regex term with the suffix i.e. xxx%s.",
						ZERO_OR_ONE_DIR_SEPARATOR_REGEX),
				regexCreated, is(expectedRegex));
	}

	@Test
	public void should_Return_A_Regex_With_A_Suffix_When_A_Path_Of_Two_Levels_Are_Passed_In() {
		String inputPath = "src/com/";
		String expectedRegex = "src/(\\w?)(/?)";

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR);
		String regexCreated = regexEngine.createRegexFromPath(inputPath);
		assertThat("Regex did not create the expected regex term with the suffix i.e. xxx/(\\w?)(/?).",
				regexCreated, is(expectedRegex));
	}

	@Test
	public void should_Return_A_Regex_With_A_Suffix_When_A_Path_Of_Three_Levels_Are_Passed_In() {
		String inputPath = "src/com/java/";
		String expectedRegex = "src/(\\w)/java(/?)";

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR);
		String regexCreated = regexEngine.createRegexFromPath(inputPath);
		assertThat("Regex did not create the expected regex term with the suffix i.e. xxx/(\\w+)/yyy(/?).",
				regexCreated, is(expectedRegex));
	}

	@Test
	public void should_Return_A_Regex_With_A_Suffix_When_A_Path_Of_Four_Levels_Are_Passed_In() {
		String inputPath = "src/com/java/lang/";
		String expectedRegex = "src/(\\w+)/lang(/?)";

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR);
		String regexCreated = regexEngine.createRegexFromPath(inputPath);
		assertThat("Regex did not create the expected regex term with the suffix i.e. xxx/(\\w+)/yyy(/?).",
				regexCreated, is(expectedRegex));
	}

	@Test
	public void should_Return_A_Regex_With_A_Suffix_When_A_Single_Level_Path_With_File_Name_Is_Passed_In() {
		String inputPath = "src/someJavaFile.java";
		String expectedRegex = "src/(\\w)";

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR);
		String regexCreated = regexEngine.createRegexFromPath(inputPath);
		assertThat("Regex did not create the expected regex term with the suffix i.e. xxx/(\\w).",
				regexCreated, is(expectedRegex));
	}

	@Test
	public void should_Return_A_PackageName_Given_A_Path_With_No_Filename_And_A_Prefix_To_Strip() {
		String inputPath = "corba/make/com/sun/corba/minclude/";
		String expectedPackageName = "com.sun.corba.minclude";
		
		List<String> prefixesToUse = new ArrayList<String>();
		prefixesToUse.add("corba/make/");
		
		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		String packageNameReturned = regexEngine.retrievePackageNameFromPath(
				inputPath, prefixesToUse);

		assertThat("Did not return expected packagename i.e. abc.def.xyz",
				packageNameReturned, is(expectedPackageName));
	}

	@Test
	public void should_Return_A_PackageName_Upto_A_Level_Given_A_Path_With_No_Filename_A_Prefix_To_Strip_And_Levels() {
		String inputPath = "corba/src/share/classes/com/sun/corba/se/internal/corba/";
		String expectedFullPackageName = "com.sun.corba.se.internal.corba";		
		String expectedPackageNameUptoLevel3 = "com.sun.corba.*";
		String expectedPackageNameUptoLevel2 = "com.sun.*";
		String expectedPackageNameUptoLevel1 = "com.*";
		String expectedPackageNameUptoLevel0 = "com.*";
		
		List<String> prefixesToUse = new ArrayList<String>();
		prefixesToUse.add("corba/src/share/classes/");
		
		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		String packageNameReturned = regexEngine.retrievePackageNameFromPath(
				inputPath, prefixesToUse);
		assertThat("Did not return expected packagename i.e. abc.def.xyz",
				packageNameReturned, is(expectedFullPackageName));
		
		packageNameReturned = regexEngine.retrievePackageNameFromPath(
				inputPath, prefixesToUse, 3);
		assertThat("Did not return expected packagename i.e. abc.def.xyz.*",
				packageNameReturned, is(expectedPackageNameUptoLevel3));

		packageNameReturned = regexEngine.retrievePackageNameFromPath(
				inputPath, prefixesToUse, 2);
		assertThat("Did not return expected packagename i.e. abc.def.*",
				packageNameReturned, is(expectedPackageNameUptoLevel2));
		
		packageNameReturned = regexEngine.retrievePackageNameFromPath(
				inputPath, prefixesToUse, 1);
		assertThat("Did not return expected packagename i.e. abc.*",
				packageNameReturned, is(expectedPackageNameUptoLevel1));
		
		packageNameReturned = regexEngine.retrievePackageNameFromPath(
				inputPath, prefixesToUse, 0);
		assertThat("Did not return expected packagename i.e. abc.*",
				packageNameReturned, is(expectedPackageNameUptoLevel0));
	}
	
	@Test
	public void should_Return_A_List_Of_PackageNames_Given_A_List_Of_Paths_With_No_Filenames_And_A_Prefix_To_Strip() {
		List<String> listOfInputPaths = new ArrayList<String>();
		listOfInputPaths.add("corba/make/com/sun/corba/minclude/");
		listOfInputPaths.add("corba/other_make/com/sun/corba/se/impl/naming/");

		List<String> expectedPackagenames = new ArrayList<String>();
		expectedPackagenames.add("com.sun.corba.minclude");
		expectedPackagenames.add("com.sun.corba.se.impl.naming");

		List<String> listOfPrefixes = new ArrayList<String>();
		listOfPrefixes.add("corba/other_make/");
		listOfPrefixes.add("corba/make/");

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		List<String> packageNamesReturned = regexEngine
				.retrievePackageNamesFromListOfPaths(listOfInputPaths, listOfPrefixes);
		assertThat(packageNamesReturned, is(expectedPackagenames));
		
		expectedPackagenames.clear();
		expectedPackagenames.add("com.sun.corba.minclude.*");
		expectedPackagenames.add("com.sun.corba.se.*");
		packageNamesReturned = regexEngine
				.retrievePackageNamesFromListOfPaths(listOfInputPaths, listOfPrefixes, 4);
		assertThat(packageNamesReturned, is(expectedPackagenames));

		expectedPackagenames.clear();
		expectedPackagenames.add("com.sun.corba.*");
		packageNamesReturned = regexEngine
				.retrievePackageNamesFromListOfPaths(listOfInputPaths, listOfPrefixes, 3);
		assertThat(packageNamesReturned, is(expectedPackagenames));
		
		expectedPackagenames.clear();
		expectedPackagenames.add("com.sun.*");
		packageNamesReturned = regexEngine
				.retrievePackageNamesFromListOfPaths(listOfInputPaths, listOfPrefixes, 2);
		assertThat(packageNamesReturned, is(expectedPackagenames));		
	}

	@Test
	public void should_Return_A_PackageName_Given_A_Path_With_Filename_And_A_Prefix_To_Strip() {
		String inputPath = "corba/src/share/classes/com/sun/corba/se/spi/legacy/connection/LegacyServerSocketManager.java";
		String expectedString = "com.sun.corba.se.spi.legacy.connection";

		List<String> prefixesToUse = new ArrayList<String>();
		prefixesToUse.add("corba/src/share/classes/");

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		String packageNameReturned = regexEngine.retrievePackageNameFromPath(
				inputPath, prefixesToUse);

		assertThat("Did not return expected packagename i.e. abc.def.xyz",
				packageNameReturned, is(expectedString));
	}

	@Test
	public void should_Return_A_List_Of_PackageNames_Given_A_List_Of_Paths_With_Filename_And_A_Prefix_To_Strip() {
		List<String> listOfInputPaths = new ArrayList<String>();
		listOfInputPaths.add("corba/src/share/classes/com/sun/corba/se/spi/legacy/connection/LegacyServerSocketManager.java");
		listOfInputPaths.add("corba/other_make/com/sun/corba/se/impl/naming/cosnaming/BindingIteratorImpl.java");

		List<String> expectedPackagenames = new ArrayList<String>();
		expectedPackagenames.add("com.sun.corba.se.impl.naming.cosnaming");
		expectedPackagenames.add("com.sun.corba.se.spi.legacy.connection");		

		List<String> listOfPrefixes = new ArrayList<String>();
		listOfPrefixes.add("corba/other_make/");
		listOfPrefixes.add("corba/src/share/classes/");

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		List<String> packageNamesReturned = regexEngine
				.retrievePackageNamesFromListOfPaths(listOfInputPaths, listOfPrefixes);
		assertThat(packageNamesReturned, is(expectedPackagenames));
	}
	
	@Test
	public void should_Return_A_Regex_Containing_PackageName_Given_A_Path_With_No_Filename_And_A_Prefix() {
		String inputPath = "corba/make/com/sun/corba/minclude/";
		String expectedString = "corba/(\\w+)/com/sun/corba/minclude(/?)";

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
	
		List<String> listOfPrefixes = new ArrayList<String>();
		listOfPrefixes.add("make/");
		
		String packageNameReturned = regexEngine
				.retrieveRegexContainingPackageNameFromPath(inputPath, listOfPrefixes);

		assertThat( "Did not return expected regex with packagename i.e. abc/(\\w+)/def/xyz/(/?)", packageNameReturned,
				is(expectedString));
	}


	@Test
	public void should_Return_A_List_Of_Regexes_Containing_PackageName_Given_A_List_Of_Paths_With_No_Filenames_And_A_Prefix() {
		List<String> listOfInputPaths = new ArrayList<String>();
		listOfInputPaths.add("corba/make/com/sun/corba/minclude/");
		listOfInputPaths.add("corba/other_make/com/sun/corba/se/impl/naming/cosnaming/");

		List<String> expectedRegexContainingPackagename = new ArrayList<String>();
		expectedRegexContainingPackagename.add("corba/(\\w+)/com/sun/corba/minclude(/?)");
		expectedRegexContainingPackagename.add("corba/(\\w+)/com/sun/corba/se/impl/naming/cosnaming(/?)");

		List<String> listOfPrefixes = new ArrayList<String>();
		listOfPrefixes.add("other_make/");
		listOfPrefixes.add("make/");

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		
		List<String> packageNameReturned = regexEngine
				.retrieveRegexContainingPackageNameFromListOfPath(listOfInputPaths, listOfPrefixes);

		assertThat( "Did not return expected regex with packagename i.e. abc/(\\w+)/def/xyz/(/?)", packageNameReturned,
				is(expectedRegexContainingPackagename));
	}

	@Test
	@Ignore
	public void shouldReturnARegexContainingPackageNameGivenAPathWithNoFilenameAndAPrefix_QuickVersion() {
		String actual = "corba/make/com/sun/corba/minclude/";
		String expected = "corba/(\\w+)/com/sun/corba/minclude(/?)";

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		
		List<String> listOfPrefixes = new ArrayList<String>();
		listOfPrefixes.add("make/");
		
		String packageNameReturned = regexEngine
				.retrieveRegexContainingPackageNameFromPath(actual, listOfPrefixes);
		assertThat(expected, is(packageNameReturned));
	}

	@Test
	public void should_Return_A_Regex_Containing_PackageName_Given_A_Path_With_Filename_And_A_Prefix() {
		String inputPath = "corba/make/com/sun/corba/minclude/com_sun_corba_se_internal_LegacyFiles.jmk";
		String expectedString = "corba/(\\w+)/com/sun/corba/minclude/(\\w)";

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		
		List<String> listOfPrefixes = new ArrayList<String>();
		listOfPrefixes.add("make/");
		
		String packageNameReturned = regexEngine
				.retrieveRegexContainingPackageNameFromPath(inputPath, listOfPrefixes);

		assertThat(
				String.format(
						"Did not return expected regex with packagename i.e. abc%s%s%sdef%sxyz%s%s",
						DEFAULT_DIRECTORY_SEPARATOR,
						ONE_OR_MORE_WORD_CHARS_REGEX,
						DEFAULT_DIRECTORY_SEPARATOR,
						DEFAULT_DIRECTORY_SEPARATOR,
						DEFAULT_DIRECTORY_SEPARATOR, ONE_WORD_CHAR_REGEX),
				packageNameReturned, is(expectedString));
	}

	@Test
	@Ignore
	public void should_Return_A_List_Of_Regexes_Containing_PackageNames_Given_A_List_Of_Paths_With_Filenames_And_A_Prefix() {
		List<String> listOfInputPaths = new ArrayList<String>();
		listOfInputPaths.add("corba/make/com/sun/corba/minclude/com_sun_corba_se_internal_LegacyFiles.jmk");
		listOfInputPaths.add("corba/other_make/com/sun/corba/se/impl/naming/cosnaming/BindingIteratorImpl.java");

		List<String> expectedRegexContainingPackagename = new ArrayList<String>();
		expectedRegexContainingPackagename.add("corba/(\\w+)/com/sun/corba/minclude/(\\w)");
		expectedRegexContainingPackagename.add("corba/(\\w+)/com/sun/corba/se/impl/naming/cosnaming/(\\w)");

		List<String> listOfPrefixes = new ArrayList<String>();
		listOfPrefixes.add("other_make/");
		listOfPrefixes.add("make/");

		RegexCreationEngine regexEngine = new RegexCreationEngine(
				DEFAULT_DIRECTORY_SEPARATOR, DEFAULT_PACKAGENAME_SEPARATOR);
		
		List<String> packageNameReturned = regexEngine
				.retrieveRegexContainingPackageNameFromListOfPath(listOfInputPaths, listOfPrefixes);

		assertThat( "Did not return expected regex with packagename i.e. abc/(\\w+)/def/xyz/(/?)", packageNameReturned,
				is(expectedRegexContainingPackagename));
	}	
}