package org.ljc.adoptojdk.converters;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.ljc.adoptojdk.converters.CommonFunctions.COMMA_SEPARATOR;

public final class CSVBehaviours {
    private CSVFile csvFile;
	List<String[]> csvLinesRead;
	String csvHeader;

    @Before
	public void setup() {
		csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFile.csv",
				COMMA_SEPARATOR);
		csvHeader = "firstField, secondField, thirdField";
	}

	@Test
	public void should_Read_A_Line_Of_CSV_Text_From_A_CSV_file()
			throws IOException {
		String expectedCSVLine = "name,age,address";

		String[] firstCSVLineReadFromFile = csvFile.readFirstLineOfCSV();
		assertThat("No data read", firstCSVLineReadFromFile.length > 0,
				is(true));
		String actualLineRead = csvFile.createCSVLineFromArray(
				firstCSVLineReadFromFile, COMMA_SEPARATOR);
		assertThat(actualLineRead, is(expectedCSVLine));
	}

	@Test
	public void should_Read_Multiple_Lines_Of_CSV_From_A_CSV_file()
			throws FileNotFoundException, IOException {
		String expectedCSVLines = "name,age,address\nharry,28,\"123 first avenue, london e33 1AB.\"";

		csvLinesRead = csvFile.readCSVfile();
		String actualLinesRead = csvFile.createCSVLinesFromListOfArrays(
				csvLinesRead, COMMA_SEPARATOR, 0, 1);
		assertThat("Issue reading multiple lines from CSV file",
				actualLinesRead, is(expectedCSVLines));
	}

	@Test
	public void should_Read_CVS_Where_Last_Field_Contains_A_List()
			throws IOException {
		String expectedCSVLineWithList = "merry,10,\"123 first avenue, \nlondon e33 1AB.\"";

		csvLinesRead = csvFile.readCSVfile();
		String actualLineWithList = csvFile.createCSVLinesFromListOfArrays(
				csvLinesRead, COMMA_SEPARATOR, 2, 2);
		assertThat("Issue reading CSV file where the last field is a list",
				actualLineWithList, is(expectedCSVLineWithList));
	}

	@Test
	public void should_Read_CVS_Where_First_Field_Contains_A_List()
			throws IOException {
		String expectedCSVLineWithList = "\"merry\nnew line\",10,\"123 first avenue, london e33 1AB.\"";

		csvLinesRead = csvFile.readCSVfile();
		String actualLineWithList = csvFile.createCSVLinesFromListOfArrays(
				csvLinesRead, COMMA_SEPARATOR, 3, 3);
		assertThat(actualLineWithList, is(expectedCSVLineWithList));
	}
}