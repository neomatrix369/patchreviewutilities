package org.ljc.adoptojdk.converters;

import models.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import static org.ljc.adoptojdk.converters.CommonFunctions.COMMA_SEPARATOR;
import static org.ljc.adoptojdk.converters.MentorFactory.addMentorOnlyIfUnique;
import static org.ljc.adoptojdk.converters.MentorFactory.createMentorClassesUsingCSVAsListOfStringsArrays;

public final class CSVToSQLBehaviours {

    private CSVFile csvFile;
	List<String[]> csvLinesRead;
	String csvHeader;

    @Before
	public void setup() {
		csvFile = new CSVFile("src/main/resources/org/ljc/adoptojdk/converters/sampleFile.csv", COMMA_SEPARATOR);
		csvHeader = "firstField, secondField, thirdField";
	}

	@Test
	public void should_Convert_A_Single_Line_Of_CSV_Text_With_Fields_And_FieldValues_To_SQL_Text() throws IOException {
		String tableName = "interest";
		String csvText = "1, '.*', 'corba'";
        String actualSQLText = CSVToSQL.convertUsing(tableName, csvText);
		String expectedSQLText = String.format("insert into %s values (1, '.*', 'corba');", tableName);
		assertThat("Did not receive the expected SQL text", actualSQLText, is(equalTo(expectedSQLText)));
    }

    @Test
    public void should_convert_a_single_line_of_CSV_text_With_Fields_And_Values_To_Multile_Table_SQL_Text() {
        String csvText = "'*.','corba','Corba','announce@glassfish-corba.java.net','List'";
        String[] tableNames = {"mentor","interest","mentor_interest"};
        String actualSQLText = CSVToSQL.convertUsing(tableNames, csvText);
        String expectedSQLText = "insert into mentor (" + CSVToSQL.MENTOR_TABLE_FIELDS +") values(1,'Corba','announce@glassfish-corba.java.net','List');\n" +
                                 "insert into interest values(1,'*.','corba');\n" +
                                 "insert into mentor_interest values(1,1);\n";
        assertThat("Did not receive the expected SQL texts", actualSQLText, is(equalTo(expectedSQLText)));
    }

	@Test
	public void should_Convert_A_Mentor_Class_Object_With_Interests_Into_SQL_Text() throws IOException {
        Mentors mentors = getMentors();

		String expectedSQLText =
                "insert into mentor (id,name,email,mentor_type) values(1,'Hotspot','hotspot-dev@openjdk.java.net','PROJECT');\n" +
                "insert into interest values(1,'.*','hotspot');\n" +
                "insert into interest values(2,'.*','corba');\n" +
                "insert into mentor_interest values(1,1);\n" +
                "insert into mentor_interest values(1,2);\n";

		String actualSQLText = CSVToSQL.convertUsing(mentors);
		assertThat(actualSQLText, is(expectedSQLText));
	}

    private Mentors getMentors() {
        Interests interests = new Interests();

        Interest interestOne = new Interest();
        interestOne.id = 1;
        interestOne.path = ".*";
        interestOne.project = "hotspot";

        Interest interestTwo = new Interest();
        interestTwo.id = 2;
        interestTwo.path = ".*";
        interestTwo.project = "corba";

        interests.add(interestOne);
        interests.add(interestTwo);

        Mentor mentor = new Mentor();
        mentor.id = 1;
        mentor.name = "Hotspot";
        mentor.email = "hotspot-dev@openjdk.java.net";
        mentor.mentorType = MentorType.PROJECT;
        mentor.interests = interests;

        Mentors mentors = new Mentors();
        mentors.add(mentor);
        return mentors;
    }

    @Test
	public void should_Create_SQL_from_A_Line_Of_CSV_Via_The_Mentor_Classes()
			throws IOException {
		String expectedSQLText = "insert into mentor (id,name,email,mentor_type) values(1,'Corba','announce@glassfish-corba.java.net','LIST');\n" +
                "insert into interest values(1,'^corba((\\/)?)','corba');\n" +
                "insert into mentor_interest values(1,1);\n";

		csvFile = new CSVFile("src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackage.csv", COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentor mentor = createMentorClassesUsingCSVAsListOfStringsArrays(csvLinesRead, 1, 1);
		Mentors mentors = new Mentors();
		mentors.add(mentor);

		String actualSQLText = CSVToSQL.convertUsing(mentors);
		assertThat(actualSQLText, is(expectedSQLText));
	}

	@Test
	public void should_Create_The_Mentor_Classes_From_CSV_Fields_With_One_Or_More_Lists()
			throws IOException {
		String expectedSQLText = "insert into mentor (id,name,email,mentor_type) values(1,'Netbeans Projects','nb-projects-dev','LIST');\n" +
                "insert into interest values(1,'\\\"\\/?netbeans\\/?\n" +
                "\\/?nbproject\\/?\\\"','netbeans');\n" +
                "insert into mentor_interest values(1,1);\n";

		csvLinesRead = new ArrayList<String[]>();
		String[] arrayOfStrings = new String[] {
				"\\\"\\/?netbeans\\/?\n"+
                "\\/?nbproject\\/?\\\"",
                "netbeans","Netbeans Projects", "nb-projects-dev", "List" };
		csvLinesRead.add(arrayOfStrings);

        Mentors mentors = getMentorsFromCSVAsListOfStringsArrays();
		String actualSQLText = CSVToSQL.convertUsing(mentors);
		assertThat(actualSQLText, is(expectedSQLText));
	}

    private Mentors getMentorsFromCSVAsListOfStringsArrays() {
        Mentor mentor = createMentorClassesUsingCSVAsListOfStringsArrays(csvLinesRead, 0, 1);
        Mentors mentors = new Mentors();
        mentors.add(mentor);
        return mentors;
    }

    @Test
	public void should_Create_SQL_from_Multiple_Lines_Of_CSV_Via_The_Mentor_Classes()
			throws IOException {
		String expectedSQLText = "insert into mentor (id,name,email,mentor_type) values(1,'Corba','announce@glassfish-corba.java.net','LIST');\n" +
                "insert into mentor (id,name,email,mentor_type) values(2,'Corba','commits@glassfish-corba.java.net','LIST');\n" +
                "insert into mentor (id,name,email,mentor_type) values(3,'Corba','dev@glassfish-corba.java.net','LIST');\n" +
                "insert into interest values(1,'^corba((\\/)?)','corba');\n" +
                "insert into mentor_interest values(1,1);\n" +
                "insert into mentor_interest values(2,1);\n" +
                "insert into mentor_interest values(3,1);\n";

        Mentors mentors = getMentorsFromListOfStringsArrays("src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackage.csv", 1, 3);
		String actualSQLText = CSVToSQL.convertUsing(mentors);
		assertThat(actualSQLText, is(expectedSQLText));
	}

    private Mentors getMentorsFromListOfStringsArrays(String csvFilename, int startIndex, int endIndex) throws IOException {
        csvFile = new CSVFile(csvFilename, COMMA_SEPARATOR);
        csvLinesRead = csvFile.readCSVfile();

        Mentors mentors = new Mentors();
        Mentor mentor;
        int idCtr = 1;
        for (int recordIndex = startIndex; recordIndex <= endIndex; recordIndex++) {
            mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
                    csvLinesRead, recordIndex, idCtr++);
            mentors.add(mentor);
        }
        return mentors;
    }

    @Test
	public void should_Not_Add_Entry_With_No_Mentor_Name() throws IOException {
        Mentors mentors = getMentorsFromListOfStringsArrays("src/main/resources/org/ljc/adoptojdk/converters/sampleFileEmptyMentorField.csv", 1, 3);
		String actualSQLText = CSVToSQL.convertUsing(mentors);
		String expectedSQLText = "insert into mentor (id,name,email,mentor_type) values(1,'Corba','announce@glassfish-corba.java.net','LIST');\n" +
            "insert into interest values(1,'^corba((\\/)?)','corba');\n" +
            "insert into mentor_interest values(1,1);\n";

		assertThat("Entry without mentor name has gone wrong", actualSQLText, is(expectedSQLText));
	}

	@Test
	public void should_Not_Add_Duplicate_Mentors_To_The_List()
			throws IOException {
		String expectedSQLText = "insert into mentor (id,name,email,mentor_type) values(1,'Corba','announce@glassfish-corba.java.net','LIST');\n" +
            "insert into mentor (id,name,email,mentor_type) values(2,'Corba','commits@glassfish-corba.java.net','LIST');\n" +
            "insert into mentor (id,name,email,mentor_type) values(3,'Corba','dev@glassfish-corba.java.net','LIST');\n" +
            "insert into interest values(1,'^corba((\\/)?)','corba');\n" +
            "insert into mentor_interest values(1,1);\n" +
            "insert into mentor_interest values(2,1);\n" +
            "insert into mentor_interest values(3,1);\n";

        Mentors mentors = getMentorsFromListOfStringsArrays(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackageDuplicates.csv",1, 5);
		String actualSQLText = CSVToSQL.convertUsing(mentors);
		assertThat("Issue when adding CSV containing duplicates", actualSQLText, is(expectedSQLText));
	}

	@Test
	public void should_Add_Unique_Interests_To_The_List() throws IOException {
		String expectedSQLText =
                "insert into mentor (id,name,email,mentor_type) values(1,'Corba','announce@glassfish-corba.java.net','LIST');\n" +
                "insert into mentor (id,name,email,mentor_type) values(2,'Corba','commits@glassfish-corba.java.net','LIST');\n" +
                "insert into mentor (id,name,email,mentor_type) values(3,'Corba','dev@glassfish-corba.java.net','LIST');\n" +
                "insert into interest values(1,'^corba((\\/)?)','corba');\n" +
                "insert into interest values(2,'^hotspot((\\/)?)','hotspot');\n" +
                "insert into interest values(3,'^build((\\/)?)','build');\n" +
                "insert into mentor_interest values(1,1);\n" +
                "insert into mentor_interest values(2,1);\n" +
                "insert into mentor_interest values(3,1);\n" +
                "insert into mentor_interest values(3,2);\n" +
                "insert into mentor_interest values(3,3);\n";

		CSVFile csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackageDuplicates.csv",
				COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentors mentors = new Mentors();
		Mentor mentor;
        int idCtr = 1;
		for (int recordIndex = 1; recordIndex <= 7; recordIndex++) {
			mentor = createMentorClassesUsingCSVAsListOfStringsArrays(csvLinesRead, recordIndex, idCtr++);
			mentors = addMentorOnlyIfUnique(mentors, mentor);
		}

		String actualSQLText = CSVToSQL.convertUsing(mentors);
		assertThat("Converted SQL does not match", actualSQLText,
				is(expectedSQLText));
	}

	@Test
	public void should_Not_Add_Entry_With_No_Interest_Name() throws IOException {
		CSVFile csvFile = new CSVFile(
                "src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackage.csv",
				COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentors mentors = new Mentors();
		csvLinesRead = csvFile.readCSVfile();

		Mentor mentor;
        int idCtr = 1;
		for (int recordIndex = 1; recordIndex <= 7; recordIndex++) {
			mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
					csvLinesRead, recordIndex, idCtr);
			mentors = addMentorOnlyIfUnique(mentors, mentor);
		}

		String actualSQLText = CSVToSQL.convertUsing(mentors);
		String expectedSQLText =
            "insert into mentor (id,name,email,mentor_type) values(1,'Corba','announce@glassfish-corba.java.net','LIST');\n" +
            "insert into mentor (id,name,email,mentor_type) values(2,'Corba','commits@glassfish-corba.java.net','LIST');\n" +
            "insert into mentor (id,name,email,mentor_type) values(3,'Corba','dev@glassfish-corba.java.net','LIST');\n" +
            "insert into interest values(1,'^corba((\\/)?)','corba');\n" +
            "insert into mentor_interest values(1,1);\n" +
            "insert into mentor_interest values(2,1);\n" +
            "insert into mentor_interest values(3,1);\n";

		assertThat(
				"Entry without mentor name has gone wrong, does not match the expected SQL",
				actualSQLText, is(expectedSQLText));
	}

    @Test
    public void should_Normalise_Interests_When_Two_Or_More_Similar_Interests_Exists_With_Different_IDs() throws IOException {
        Mentors mentors = getMentors();
        List<Interest> interests = getAnotherInterest();
        Mentor mentor = getAnotherMentor(interests);
        mentors.add(mentor);

        String actualSQLText = CSVToSQL.convertUsing(mentors);
        String expectedSQLText =
            "insert into mentor (id,name,email,mentor_type) values(1,'Hotspot','hotspot-dev@openjdk.java.net','PROJECT');\n" +
            "insert into mentor (id,name,email,mentor_type) values(2,'Nashorn','Nashorn@openjdk.java.net','PROJECT');\n" +
            "insert into interest values(1,'.*','hotspot');\n" +
            "insert into interest values(2,'.*','corba');\n" +
            "insert into mentor_interest values(1,1);\n" +
            "insert into mentor_interest values(1,2);\n" +
            "insert into mentor_interest values(2,1);\n";

        assertThat("Interests have not been normalised as expected",
                actualSQLText, is(expectedSQLText));
    }

    private Mentor getAnotherMentor(List<Interest> interests) {
        Mentor mentor = new Mentor();
        mentor.id = 2;
        mentor.name = "Nashorn";
        mentor.email = "Nashorn@openjdk.java.net";
        mentor.mentorType = MentorType.PROJECT;
        mentor.interests = interests;
        return mentor;
    }

    private List<Interest> getAnotherInterest() {
        List<Interest> interests = new Interests();
        Interest interestOne = new Interest();
        interestOne.id = 2;
        interestOne.path = ".*";
        interestOne.project = "hotspot";
        interests.add(interestOne);
        return interests;
    }

    @Test @Ignore
    public void should_Normalise_Mentors_When_Two_Or_More_Similar_Mentors_Exists_With_Different_IDs()  {
        Interests interests = new Interests();
        Mentors mentors = new Mentors();

        Interest interestOne = new Interest();
        interestOne.id = 1;
        interestOne.path = ".*";
        interestOne.project = "hotspot";

        Interest interestTwo = new Interest();
        interestTwo.id = 2;
        interestTwo.path = ".*";
        interestTwo.project = "corba";

        interests.add(interestOne);
        interests.add(interestTwo);

        Mentor mentor = new Mentor();
        mentor.id = 1;
        mentor.name = "Hotspot";
        mentor.email = "hotspot-dev@openjdk.java.net";
        mentor.mentorType = MentorType.PROJECT;
        mentor.interests = interests;

        mentors.add(mentor);

        mentor = new Mentor();
        mentor.id = 2;
        mentor.name = "Hotspot";
        mentor.email = "hotspot-dev@openjdk.java.net";
        mentor.mentorType = MentorType.PROJECT;
        mentor.interests = interests;
        mentors.add(mentor);

        //System.out.println(mentors);

        String actualSQLText = CSVToSQL.convertUsing(mentors);
        String expectedSQLText =
                "insert into mentor (id,name,email,mentor_type) values(1,'Hotspot','hotspot-dev@openjdk.java.net','PROJECT');\n" +
                        "insert into interest values(1,'.*','hotspot');\n" +
                        "insert into interest values(2,'.*','corba');\n" +
                        "insert into mentor_interest values(1,1);\n" +
                        "insert into mentor_interest values(1,2);\n";

        assertThat("Mentors have not been normalised as expected",
                actualSQLText, is(expectedSQLText));
    }
}