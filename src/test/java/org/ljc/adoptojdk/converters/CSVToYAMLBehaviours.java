package org.ljc.adoptojdk.converters;

import models.*;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.ljc.adoptojdk.converters.CommonFunctions.COMMA_SEPARATOR;
import static org.ljc.adoptojdk.converters.MentorFactory.addMentorOnlyIfUnique;
import static org.ljc.adoptojdk.converters.MentorFactory.createMentorClassesUsingCSVAsListOfStringsArrays;

public final class CSVToYAMLBehaviours {
    private static final String LIST_OF_REPLACEMENTS[][] = new String[][] {
            {"!models.Mentors", "Mentors:"}, {"!models.Interests", ""}, {"!", "!!"}};

    private CSVFile csvFile;
	List<String[]> csvLinesRead;
	String csvHeader;

    @Before
	public void setup() {
		csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFile.csv",
				COMMA_SEPARATOR);
		csvHeader = "firstField, secondField, thirdField";
	}

	@Test
	public void should_Convert_A_Single_Line_Of_CSV_Text_With_Headers_Only_To_YAML_Text()
			throws IOException {
		String className = "sample";
		String csvText = "";
		String actual = CSVToYAML.convertUsing(className, csvHeader, csvText, LIST_OF_REPLACEMENTS);
		String expectedYAMLText = "!!sample\nfirstField: \n secondField: \n thirdField: \n";
		assertThat(actual, is(expectedYAMLText));
	}

	@Test
	public void should_Convert_A_Single_Line_Of_CSV_Text_With_Headers_And_Field_Values_To_YAML_Text()
			throws IOException {
		String className = "sample";
		String csvText = "merry,21,london";
		String actualYAMLText = CSVToYAML.convertUsing(className, csvHeader,
				csvText, LIST_OF_REPLACEMENTS);
		String expectedYAMLText = "!!sample\nfirstField: merry\n secondField: 21\n thirdField: london\n";
		// System.out.format("%s%n", actualYAMLText);
		// System.out.format("%s", expectedYAMLText);
		assertThat(actualYAMLText, is(expectedYAMLText));
	}

	@Test
	public void should_Convert_An_Mentor_Class_Object_Without_Interests_Into_YAML_Text()
			throws IOException {
		Mentor mentor = new Mentor();
        mentor.id = 1;
		mentor.name = "Hotspot";
		mentor.email = "hotspot-dev@openjdk.java.net";
		mentor.mentorType = MentorType.PROJECT;

		Mentors mentors = new Mentors();
		mentors.add(mentor);

		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Hotspot\n   email: hotspot-dev@openjdk.java.net\n   mentorType: PROJECT\n";
		String actualYAMLText = CSVToYAML.convertUsing(Mentors.class, mentors,
				null, LIST_OF_REPLACEMENTS);
		assertThat(actualYAMLText, is(expectedYAMLText));
	}

	@Test
	public void should_Convert_A_Mentor_Class_Object_With_Interests_Into_YAML_Text()
			throws IOException {
		Interests interests = new Interests();

		Interest interestOne = new Interest();
        interestOne.id = 1;
		interestOne.path = ".*";
		interestOne.project = "hotspot";

		Interest interestTwo = new Interest();
        interestTwo.id = 2;
		interestTwo.path = ".*";
		interestTwo.project = "corba";

		interests.add(interestOne);
		interests.add(interestTwo);

		Mentor mentor = new Mentor();
        mentor.id = 1;
		mentor.name = "Hotspot";
		mentor.email = "hotspot-dev@openjdk.java.net";
		mentor.mentorType = MentorType.PROJECT;
		mentor.interests = interests;

		Mentors mentors = new Mentors();
		mentors.add(mentor);

		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Hotspot\n   email: hotspot-dev@openjdk.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: .*\n      project: hotspot\n   - !!models.Interest\n      id: 2\n      path: .*\n      project: corba\n   mentorType: PROJECT\n";
		String actualYAMLText = CSVToYAML.convertUsing(Mentors.class, mentors,
				null, LIST_OF_REPLACEMENTS);
		// System.out.format("%s%n", actualYAMLText);
		// System.out.format("%s", expectedYAMLText);
		assertThat(actualYAMLText, is(expectedYAMLText));
	}

	@Test
	public void should_Create_YAML_from_A_Line_Of_CSV_Via_The_Mentor_Classes()
			throws IOException {
		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Corba\n   email: announce@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n";

		csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackage.csv",
				COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentor mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
				csvLinesRead, 1, 1);
		Mentors mentors = new Mentors();
		mentors.add(mentor);

		String actualYAMLText = CSVToYAML.convertUsing(Mentors.class, mentors,
				null, LIST_OF_REPLACEMENTS);
		assertThat(actualYAMLText, is(expectedYAMLText));
	}

	@Test
	public void should_Create_The_Mentor_Classes_From_CSV_Fields_With_One_Or_More_Lists()
			throws IOException {
		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Netbeans Projects\n   email: nb-projects-dev\n   interests: \n   - !!models.Interest\n      id: 1\n      path: |-\n         \"\\/?netbeans\\/?\n         \\/?nbproject\\/?\"\n      project: netbeans\n   mentorType: LIST\n";

		csvLinesRead = new ArrayList<String[]>();
		String[] arrayOfStrings = new String[] {
				"\"\\/?netbeans\\/?\n\\/?nbproject\\/?\"", "netbeans",
				"Netbeans Projects", "nb-projects-dev", "List" };
		csvLinesRead.add(arrayOfStrings);

		Mentor mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
				csvLinesRead, 0, 1);
		Mentors mentors = new Mentors();
		mentors.add(mentor);

		String actualYAMLText = CSVToYAML
				.convertUsing(
						Mentors.class,
						mentors,
						"src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackageLists.yml",
                        LIST_OF_REPLACEMENTS);
		assertThat(actualYAMLText, is(expectedYAMLText));
	}

	@Test
	public void should_Create_YAML_from_Multiple_Lines_Of_CSV_Via_The_Mentor_Classes()
			throws IOException {
		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Corba\n   email: announce@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n- !!models.Mentor\n   id: 2\n   name: Corba\n   email: commits@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n- !!models.Mentor\n   id: 3\n   name: Corba\n   email: dev@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n";

		csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackage.csv",
				COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentors mentors = new Mentors();
		Mentor mentor;
        int idCtr = 1;
		for (int recordIndex = 1; recordIndex <= 3; recordIndex++) {
			mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
					csvLinesRead, recordIndex, idCtr++);
			mentors.add(mentor);
		}
		String actualYAMLText = CSVToYAML.convertUsing(Mentors.class, mentors,
				null, LIST_OF_REPLACEMENTS);
		assertThat(actualYAMLText, is(expectedYAMLText));
	}

	@Test
	public void should_Not_Add_Entry_With_No_Mentor_Name() throws IOException {
		CSVFile csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFileEmptyMentorField.csv",
				COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentors mentors = new Mentors();
		Mentor mentor;
		for (int recordIndex = 1; recordIndex <= 3; recordIndex++) {
			mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
					csvLinesRead, recordIndex, 1);
			if (mentor != null) {
				mentors.add(mentor);
			}
		}
		String actualYAMLText = CSVToYAML.convertUsing(Mentors.class, mentors,
				null, LIST_OF_REPLACEMENTS);
		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Corba\n   email: announce@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n";
		assertThat("Entry without mentor name has gone wrong", actualYAMLText,
				is(expectedYAMLText));
	}

	@Test
	public void should_Not_Add_Duplicate_Mentors_To_The_List()
			throws IOException {
		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Corba\n   email: announce@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n- !!models.Mentor\n   id: 2\n   name: Corba\n   email: commits@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n- !!models.Mentor\n   id: 3\n   name: Corba\n   email: dev@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n";

		CSVFile csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackageDuplicates.csv",
				COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentors mentors = new Mentors();
		Mentor mentor;
        int idCtr = 1;
		for (int recordIndex = 1; recordIndex <= 5; recordIndex++) {
            mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
					csvLinesRead, recordIndex, idCtr);
			mentors = addMentorOnlyIfUnique(mentors, mentor);
		}

		String actualYAMLText = CSVToYAML.convertUsing(Mentors.class, mentors,
				null, LIST_OF_REPLACEMENTS);
		assertThat("Issue when adding CSV containing duplicates",
				actualYAMLText, is(expectedYAMLText));
	}

	@Test
	public void should_Add_Unique_Interests_To_The_List() throws IOException {
		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Corba\n   email: announce@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n- !!models.Mentor\n   id: 2\n   name: Corba\n   email: commits@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n- !!models.Mentor\n   id: 3\n   name: Corba\n   email: dev@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   - !!models.Interest\n      id: 2\n      path: ^hotspot((\\/)?)\n      project: hotspot\n   - !!models.Interest\n      id: 3\n      path: ^build((\\/)?)\n      project: build\n   mentorType: LIST\n";

		CSVFile csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFileJavaPackageDuplicates.csv",
				COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentors mentors = new Mentors();
		Mentor mentor;
        int idCtr = 1;
		for (int recordIndex = 1; recordIndex <= 7; recordIndex++) {
			mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
					csvLinesRead, recordIndex, idCtr++);
			mentors = addMentorOnlyIfUnique(mentors, mentor);
		}

		String actualYAMLText = CSVToYAML.convertUsing(Mentors.class, mentors,
				null, LIST_OF_REPLACEMENTS);
		assertThat("Converted YAML does not match", actualYAMLText,
				is(expectedYAMLText));
	}

	@Test
	public void should_Not_Add_Entry_With_No_Interest_Name() throws IOException {
		CSVFile csvFile = new CSVFile(
				"src/main/resources/org/ljc/adoptojdk/converters/sampleFileEmptyInterestField.csv",
				COMMA_SEPARATOR);
		csvLinesRead = csvFile.readCSVfile();

		Mentors mentors = new Mentors();
		csvLinesRead = csvFile.readCSVfile();

		Mentor mentor;
        int idCtr = 1;
		for (int recordIndex = 1; recordIndex <= 7; recordIndex++) {
			mentor = createMentorClassesUsingCSVAsListOfStringsArrays(
					csvLinesRead, recordIndex, idCtr);
			mentors = addMentorOnlyIfUnique(mentors, mentor);
		}

		String actualYAMLText = CSVToYAML.convertUsing(Mentors.class, mentors,
				null, LIST_OF_REPLACEMENTS);
		String expectedYAMLText = "Mentors:\n- !!models.Mentor\n   id: 1\n   name: Corba\n   email: announce@glassfish-corba.java.net\n   interests: \n   - !!models.Interest\n      id: 1\n      path: ^corba((\\/)?)\n      project: corba\n   mentorType: LIST\n";
		assertThat(
				"Entry without mentor name has gone wrong, does not match the expected YAML",
				actualYAMLText, is(expectedYAMLText));
	}
}