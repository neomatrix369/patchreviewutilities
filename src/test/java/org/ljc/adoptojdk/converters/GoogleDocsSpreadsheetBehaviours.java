package org.ljc.adoptojdk.converters;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.*;

import java.util.Map;
import org.junit.Test;
import com.google.gdata.util.AuthenticationException;

public class GoogleDocsSpreadsheetBehaviours {

	/**
	 * Google drive API (Google Docs API is deprecated)
	 * https://developers.google.com/drive/quickstart-java
	 * https://developers.google.com/drive/examples/java
	 * 
	 * Google Spreadsheets API version 3.0:
	 * https://developers.google.com/google-apps/spreadsheets/ Sample: Code:
	 * https
	 * ://code.google.com/p/gdata-java-client/source/browse/trunk/java/#java
	 * %2Fsample%2Fspreadsheet%253Fstate%253Dclosed Properties:
	 * https://code.google
	 * .com/p/gdata-java-client/source/browse/trunk/java/build
	 * -samples/build.properties
	 * 
	 * @throws AuthenticationException
	 */
	@Test
	public void should_Return_Cell_Contents_For_A_Given_Row_Or_Col_In_A_Worksheet_In_A_Spreadsheet()
			throws AuthenticationException {
		String expectedColumnName = "Repositories_OR_Components";
		String expectedColumnValue = "corba";
		String cellRow = "2";
		String cellCol = "B";
		Map.Entry<String, String> actual = GoogleDocsSpreadsheet
				.getCellContent(cellRow, cellCol);

		assertThat(actual.getKey(), is(expectedColumnName));
		assertThat(actual.getValue(), is(expectedColumnValue));
	}

	public void should_Login_Into_Google_Spreadsheets()
			throws AuthenticationException {
		Map<String, String> authDetails = GoogleDocsSpreadsheet
				.getUserAuthDetails();
		Boolean actual = GoogleDocsSpreadsheet.login(
				authDetails.get("username"), authDetails.get("password"));
		Boolean expected = true;
		assertThat(actual, is(expected));
	}

}
