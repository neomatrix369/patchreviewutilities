package org.ljc.adoptojdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import static org.ljc.adoptojdk.DirectoryFileUtility.*;
import static org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants.*;

import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.*;

public final class DirectoryFileUtilityBehaviours {

	@Test
	public void remove_existing_trailing_dropTrailingSeparator() {
		assertThat(dropTrailingSeparator("file/", "/"), is("file"));
	}

	@Test
	public void do_nothing_if_no_trailing_dropTrailingSeparator() {
		assertThat(dropTrailingSeparator("file", "/"), is("file"));
	}

	@Test
	public void return_empty_if_only_trailing_dropTrailingSeparator() {
		assertThat(dropTrailingSeparator("/", "/"), is(""));
	}

	@Test
	public void should_not_fail_when_empty_string_is_passed_to_dropTrailingSeparator() {
		assertThat(dropTrailingSeparator("", "/"), is(""));
	}
	
	@Test
	public void should_remove_two_chars_if_separator_has_two_chars_dropTrailingSeparator() {
		assertThat(dropTrailingSeparator("fileXX", "XX"), is("file"));
	}

	//

	@Test
	public void return_true_if_absolute_dir_with_trailing_hasTrailingSeparator() {
		assertThat(hasTrailingSeparator("/root/dir/", "/"), is(true));
	}

	@Test
	public void return_true_if_relative_dir_with_trailing_hasTrailingSeparator() {
		assertThat(hasTrailingSeparator("root/dir/", "/"), is(true));
	}

	@Test
	public void return_true_if_only_trailing_hasTrailingSeparator() {
		assertThat(hasTrailingSeparator("/", "/"), is(true));
	}

	@Test
	public void should_not_fail_when_empty_string_is_passed_to_hasTrailingSeparator() {
		assertThat(hasTrailingSeparator("", "/"), is(false));
	}

	// 
	
	@Test
	public void same_strings_means_not_removed_filenameHasBeenRemoved() {
		assertThat(filenameHasBeenRemoved("file", "file"), is(false));
	}

	
	@Test
	public void same_strings_with_trailing_separator_means_not_removed_filenameHasBeenRemoved() {
		assertThat(filenameHasBeenRemoved("file/", "file/"), is(false));
	}

	//
	
	@Test
	public void should_Sort_A_List_Of_Items_By_Length_When_An_Unsorted_List_Is_Passed_In() {
		List<String> actualUnsortedItems = new ArrayList<String>();
		actualUnsortedItems.add("corba/src/windows/");
		actualUnsortedItems.add("corba/makefiles/");
		actualUnsortedItems.add("corba/src/share/classes/");
		actualUnsortedItems.add("corba/make/");		
		actualUnsortedItems.add("corba/src/");
		actualUnsortedItems.add("corba/src/windows/resource/");
		actualUnsortedItems.add("corba/src/share/");
		actualUnsortedItems.add("corba/");
		
		List<String> expectedSortedItems = new ArrayList<String>();
		expectedSortedItems.add("corba/src/windows/resource/");
		expectedSortedItems.add("corba/src/share/classes/");
		expectedSortedItems.add("corba/src/windows/");
		expectedSortedItems.add("corba/makefiles/");
		expectedSortedItems.add("corba/src/share/");
		expectedSortedItems.add("corba/make/");		
		expectedSortedItems.add("corba/src/");
		expectedSortedItems.add("corba/");
		
		List<String> sortedItemsList = sortGiven(actualUnsortedItems, inDescendingOrderByLength());
		
		assertThat(sortedItemsList, is((expectedSortedItems)));
	}
	
	@Test
	public void should_Remove_Duplicates_When_A_List_Of_Items_Is_Passed_In() {
		List<String> actualDuplicateItems = new ArrayList<String>();
		actualDuplicateItems.add("corba/src/windows/");
		actualDuplicateItems.add("corba/src/share/classes/");
		actualDuplicateItems.add("corba/makefiles/");
		actualDuplicateItems.add("corba/src/share/classes/");
		actualDuplicateItems.add("corba/make/");		
		actualDuplicateItems.add("corba/src/windows/resource/");		
		actualDuplicateItems.add("corba/src/");
		actualDuplicateItems.add("corba/make/");		
		actualDuplicateItems.add("corba/src/windows/resource/");
		actualDuplicateItems.add("corba/src/share/");
		actualDuplicateItems.add("corba/src/windows/resource/");			
		actualDuplicateItems.add("corba/");
		
		List<String> expectedItems = new ArrayList<String>();
		expectedItems.add("corba/src/windows/");		
		expectedItems.add("corba/src/share/classes/");
		expectedItems.add("corba/makefiles/");
		expectedItems.add("corba/make/");				
		expectedItems.add("corba/src/windows/resource/");			
		expectedItems.add("corba/src/");
		expectedItems.add("corba/src/share/");
		expectedItems.add("corba/");
		Collections.sort(expectedItems);
		
		List<String> removedDuplicateItems = new ArrayList<String>();
		removedDuplicateItems = removeDuplicateItems(actualDuplicateItems);
		Collections.sort(removedDuplicateItems);
		
		assertThat(removedDuplicateItems, is(expectedItems));
	}
	
	@Test 
	public void should_Indicate_InvalidPath_Is_Passed_In() {
		String someInvalidPath = "/sds$dfszz\\eserser";
		boolean result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));
		
		
		someInvalidPath = "/sds$dfszz/eserser\n";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser\r";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));
		
		someInvalidPath = "/sds$dfszz/eserser\t";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszzeserser\\0";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser\f";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser`";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser?";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser*";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser<";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser>";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser|";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser\"";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));

		someInvalidPath = "/sds$dfszz/eserser:";
		result = isInvalidPath(someInvalidPath);
		assertThat(true, is(result));
	}

	//
	
	@Test 
	public void empty_string_returns_nothing_extractFilename() {
		assertThat(extractFilename(""), is(""));
	}

	@Test
	public void root_dir_returns_no_filename_extractFilename() {
		assertThat(extractFilename("/"), is(""));
	}

	@Test
	public void no_separator_at_the_end_returns_last_path_part_extractFilename() {
		assertThat(extractFilename("/filename"), is("filename"));
	}

	@Test
	@Ignore
	public void separator_at_the_end_returns_no_filename_extractFilename() {
		assertThat(extractFilename("filename/"), is(""));
	}

	@Test 
	@Ignore
	public void should_remove_nothing_when_path_without_filename_remoteFilenameFromPath() {
		assertThat(removeFilenameFromPath("someDirectory/", "/"), is("someDirectory/"));
	}

	@Test 
	public void should_remove_filename_from_a_path_with_filename_remoteFilenameFromPath() {
		String returnedPathWithoutFilename = removeFilenameFromPath("someDirectory/someFile.ext", DEFAULT_DIRECTORY_SEPARATOR);
		assertThat(returnedPathWithoutFilename, is("someDirectory/"));
	}

	//

	@Test 
	public void should_Sort_A_List_Of_Items_In_Asc_Order_Passed_In() {
		
		List<String> unSortedItems = new ArrayList<String>();
		unSortedItems.add("corba/src/windows/");
		unSortedItems.add("corba/src/share/classes/");
		unSortedItems.add("corba/makefiles/");
		unSortedItems.add("corba/make/");		
		
		List<String> expectedItems = new ArrayList<String>();
		expectedItems.add("corba/make/");		
		expectedItems.add("corba/makefiles/");
		expectedItems.add("corba/src/share/classes/");
		expectedItems.add("corba/src/windows/");
		
		List<String> sortedItems = new ArrayList<String>();
		sortedItems = sortGiven(unSortedItems, inAscendingOrderByName());
		
		assertThat(sortedItems, is(expectedItems));
	}
	
	@Test 
	public void should_Sort_A_List_Of_Items_In_Desc_Order_By_Token_Length() {		
		List<String> unSortedItems = new ArrayList<String>();
		unSortedItems.add("corba/src/windows/");
		unSortedItems.add("corba/src/share/classes/");
		unSortedItems.add("corba/makefiles/");
		unSortedItems.add("corba/make/");		
		
		List<String> expectedItems = new ArrayList<String>();
		expectedItems.add("corba/src/share/classes/");
		expectedItems.add("corba/src/windows/");
		expectedItems.add("corba/makefiles/");
		expectedItems.add("corba/make/");

		List<String> sortedItems = new ArrayList<String>();
		sortedItems = sortGiven(unSortedItems, inDescendingOrderByLength());
		
		assertThat(sortedItems, is(expectedItems));
	}	
}