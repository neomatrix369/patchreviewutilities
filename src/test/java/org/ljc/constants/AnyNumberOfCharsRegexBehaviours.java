package org.ljc.constants;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants;

public final class AnyNumberOfCharsRegexBehaviours extends AbstractRegexBehaviours {

	protected String getRegex() {
		return RegexCreationEngineConstants.ANY_NUMBER_OF_CHARS_REGEX;
	}

	@Test
	public void should_match_empty() {
		assertTrue(matches(""));
	}

	@Test
	public void should_match_character() {
		assertTrue(matches("F"));
	}

	@Test
	public void should_match_more_than_one_character() {
		assertTrue(matches("FF"));
	}

	@Test
	public void should_match_string_with_directory_separator() {
		assertTrue(matches("FF/FF"));
	}

}
