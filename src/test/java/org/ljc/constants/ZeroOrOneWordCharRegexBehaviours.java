package org.ljc.constants;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants;

public class ZeroOrOneWordCharRegexBehaviours extends AbstractRegexBehaviours {

	protected String getRegex() {
		return RegexCreationEngineConstants.ZERO_OR_ONE_WORD_CHAR_REGEX;
	}

	@Test
	public void should_match_empty() {
		assertTrue(matches(""));
	}

	@Test
	public void should_match_character() {
		assertTrue(matches("F"));
	}

	@Test
	public void should_not_match_more_than_one_character() {
		assertFalse(matches("FF"));
	}

	@Test
	public void should_not_match_directory_separator() {
		assertFalse(matches("/"));
	}

}
