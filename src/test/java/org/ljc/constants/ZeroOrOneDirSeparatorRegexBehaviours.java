package org.ljc.constants;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.ljc.adoptojdk.regexstuff.RegexCreationEngineConstants;

public class ZeroOrOneDirSeparatorRegexBehaviours extends AbstractRegexBehaviours {

	protected String getRegex() {
		return RegexCreationEngineConstants.ZERO_OR_ONE_DIR_SEPARATOR_REGEX;
	}

	@Test
	public void should_match_empty() {
		assertTrue(matches(""));
	}

	@Test
	public void should_not_match_not_directory_separator() {
		assertFalse(matches("F"));
	}

	@Test
	public void should_not_match_more_than_one_character() {
		assertFalse(matches("FF"));
	}

	@Test
	public void should_match_directory_separator() {
		assertTrue(matches("/"));
	}

	@Test
	public void should_not_match_string_with_directory_separator() {
		assertFalse(matches("FF/FF"));
	}

}
