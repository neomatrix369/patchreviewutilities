package org.ljc.constants;

import java.util.regex.Pattern;

public abstract class AbstractRegexBehaviours {

	protected abstract String getRegex();

	/* default */boolean matches(String input) {
		return Pattern.matches(getRegex(), input);
	}

}
